#include <iostream>

using namespace std;

struct student
{
	char fi;
	char li;
	int age;
	float gpa;
};

struct test
{
	int thing;
	char other;
	student person;
};

void strncat(char * dest, const char * source, int count)
{
	while(*dest)
		dest++;
	while(*source && count-- > 0)
	{
		*dest = *source;
		dest++;
		source++;
	}
	*dest = 0;
}

void strcat(char * dest, const char * source)
{
	while(*dest)
		dest++;
	while(*source)
	{
		*dest = *source;
		dest++;
		source++;
	}
	*dest = 0;
}

int strlen(const char * source)
{
	int length = 0;
	while(source[length++]);
	return length;
}

void sum(int t[])
{
	
}


void swap(char ** a, char ** b)
{
	char * temp = *a;
	*a = *b;
	*b = temp;
}

int main(int argc, char * argv[])
{
	test joe = {1,'c', {'j','s',19,3.0}};

	cout << joe.person.age << endl;
	//char test[2][8] = {"Hello", "World"};
	//swap(test[0], test[1]);
	//cout << test[1] << endl;
	if(argc > 2)
		cout << argv[1] << " " << argv[2] << endl;
	return 1;
}
