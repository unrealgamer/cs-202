/***************************************************************/
/* PROGRAM: Project 1 Card Shuffler                                                                  */
/*                                                                                       */
/* AUTHOR:  Shane Melton                                                                    */
/*                                                                                       */
/* DATE:    September 4, 2014                                                                        */
/*                                                                                      */
/* REVISIONS: 1                                                                */
/*                                                                                      */
/* PURPOSE: A small program that will read in 52 cards from a file, shuffle the 
			cards, print the unshuffled deck to the screen, and print the shuffled 
			deck to a new file.                                                                  */
/*                                                                                      */
/**************************************************************/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

// Constants for the Rows (suits), Columns (ranks), Cards (52 in a deck), and Max Characters allowed in a card, respectively 
const int ROWS = 4;
const int COLS = 13;
const int CARDS = 52;
const int MAX_CHARS = 14;


// Function Prototypes

/**
	Asks a user for an input file and then procedes to read the file and fill the provided array with cards.
	@param out The 3D array to populate the unshuffled cards
	@return True on success, False on failure
*/
bool readUnShuffledDeck(char (&out)[ROWS][COLS][MAX_CHARS]);

/**
	Uses the provided unshuffled deck (3D Array) to populate the shuffled deck (2D Array) in a random order.
	@param unShuffled The 3D array to use to populate the shuffled deck
	@param shuffled The 2D array to populate with cards in a random order	
	@return True on successful shuffle, and false on failure
*/
bool shuffleDeck(const char (&unShuffled)[ROWS][COLS][MAX_CHARS], char (&shuffled)[CARDS][MAX_CHARS]);

/**
	Prints the menu to the console. (Does not wait for any user input)
*/
void printMenu();

/**
	Prints the provided unshuffled deck (3D Array) to the console one line at a time.
	@param deck The 3D array that represents an unshuffled deck of cards.
*/
void printUnShuffledDeck(const char (&deck)[ROWS][COLS][MAX_CHARS]);

/**
	Asks the user for an output file location and procedes to print the provided array of cards to the file.
	@param deck The 2D array that represents a shuffled deck of cards
*/
void printSuffeledDeckToFile(const char (&deck)[CARDS][MAX_CHARS]);

/**
	Concatenates two C style strings together which is stored in the dest argument
	@param dest The destination for the concatenation and the first portion of the concatenated string
	@param source The source for the concatentation that will be appended to the destination string
*/
void strcat(char * dest, const char * source);

/**
	Copys the conents from the source C string to the destination C string
	@param dest The destination for the source to be copied too
	@param source The source to be copied	
*/
void strcopy(char * dest, const char * source);

/**
	Swaps two C style strings that are within an array
	@param a The first string to swap with the second
	@param b The second string to swap with the first
*/
void swap(char ** a, char ** b);

// End Function Prototypes

using namespace std;

// Main entry point of the program
int main() {

	// Integer used to store the users selection in the menus
	int userSel = 0;

	// Boolean to check if the unshuffled deck was loaded succesfully
	bool loadedDeck = false;

	bool deckShuffled = false;

	// 3D and 2D arrays for the unshuffled and shuffled decks respectively
	char unShuffledDeck[ROWS][COLS][MAX_CHARS];
	char shuffledDeck[CARDS][MAX_CHARS];

	while(userSel != 5)
	{
		printMenu(); // Print the menu to the screen

		cin >> userSel; // Wait for the user's input

		if(!cin.good()) // Verify the user's input is an integer
		{
			cout << "Improper Selection! Please try again!" << endl;

			cin.clear(); // Clear the input buffer

			cin.ignore(); // Flush the new line character

			continue;
		}

		cin.ignore(); // Flush the new line character '\n' from the buffer

		switch(userSel)
		{
			case 1: // Read the Unshuffled Deck from the file
			{
				loadedDeck = readUnShuffledDeck(unShuffledDeck);
				cout << endl;
				break;
			}
			case 2: // Print the Unshuffled Deck to the console 
			{
				if(loadedDeck)
					printUnShuffledDeck(unShuffledDeck);
				else
					cout << "Please load the unshuffled deck first. (Option 1)" << endl;
				break;
			}
			case 3: // Shuffle the deck
			{
				if(loadedDeck)
					deckShuffled = shuffleDeck(unShuffledDeck, shuffledDeck);
				else
					cout << "Please load the unshuffled deck first. (Option 1)" << endl;
				break;
			}
			case 4: // Print the shuffled deck to file
			{
				if(loadedDeck && deckShuffled)
					printSuffeledDeckToFile(shuffledDeck);
				else
					cout << "Please shuffle the deck first. (Option 3)" << endl;
				break;
			}
			case 5: // Exit the program
			{
				cout << "Good Bye" << endl;
				return 0;
			}
			default: // Out of range option
			{
				cout << "Please select an option 1-5." << endl;
				break;
			}
		}
		cout << "Press the enter key to continue...";
		cin.ignore();
	}
	return 0;
}


void printMenu()
{
	cout << endl << endl << "Select an option below: " << endl;

	cout << "1. Load Card File" << endl;
	cout << "2. Print Unshuffled Deck to Screen" << endl;
	cout << "3. Shuffle Deck" << endl;
	cout << "4. Print Shuffled Deck to File" << endl;
	cout << "5. Exit" << endl;

	cout << "Selection: ";
}

void printUnShuffledDeck(const char (&deck)[ROWS][COLS][MAX_CHARS])
{
	for(int r = 0; r < ROWS; r++)
	{
		for(int c = 0; c < COLS; c++)
		{
			cout << deck[r][c] << endl; //Print out each card
		}
	}
}

void printSuffeledDeckToFile(const char (&deck)[CARDS][MAX_CHARS])
{
	// Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the output file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ofstream ofs (fileName);

	// Verify the file opened correctly
	if(!ofs.is_open())
	{
		cout << "There was an error opening the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return;
	}
	// Loop through each card and print it to the file
	for(int i = 0; i < CARDS; i++)
	{
		ofs << deck[i] << endl;
	}

	ofs.close(); // Close the file

	cout << "File Succesfully Written (" << fileName << ")" << endl;
}

bool readUnShuffledDeck(char (&out)[ROWS][COLS][MAX_CHARS])
{
	//Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the input file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	// Loop through each row and column of the input file
	for(int r = 0; r < ROWS; r++)
	{
		for(int c = 0; c < COLS; c++)
		{
			ifs >> out[r][c];	 		// Read the rank into the unshuffled deck
			char suit[MAX_CHARS]; 		// A temporary placeholder for the suit
			ifs >> suit; 				// Read the rank into the placeholder
			strcat(out[r][c], " "); 	// Concatenate a space to the current card in the deck
			strcat(out[r][c], suit); 	// Concatenate the suit to the current card in the deck
		}
	}

	//Close the file
	ifs.close();

	cout << "File Succesfully Read! ";

	return true; //Success
}


bool shuffleDeck(const char (&unShuffled)[ROWS][COLS][MAX_CHARS], char (&shuffled)[CARDS][MAX_CHARS])
{
	//Populate the shuffled deck array
	for(int i = 0; i < CARDS; i++)
	{
		// i/COLS will provide an integer between 0-4 for the suit (row)
		// i%COLS will provide an integer between 0-12 for the rank (col)
		// Copy from the unshuffled array to the yet to be shuffled array
		strcopy(shuffled[i], unShuffled[i/COLS][i%COLS]); 
	}

	// Seed the random generator
	srand(time(0));

	// Loop through each card and shuffle it with a random index to swap ("shuffle") the cards
	for(int i = 0; i < CARDS; i++)
	{
		int j = rand()%CARDS; 			// Random index to shuffle
		swap(shuffled[i], shuffled[j]); // Actually swap the two cards
	}

	cout << "Deck Succesfully Shuffled!" << endl;

}

void swap( char ** a, char ** b)
{
	char * temp = *a;	// Create 'a' temp to store the pointer to 'a'
	*a = *b;			// Set 'a' pointer equal to 'b'
	*b = temp; 			// Set 'b' pointer equal to temp (which is the original 'a')
}

void strcat(char * dest, const char * source)
{
	// Move the destination pointer to the end of the C string
	while(*dest) // '\0' character will evalulate to false
		dest++; 
	// Start the source at the beginning 
	while(*source)
	{
		*dest = *source; // Copy the source character to the destination
		dest++; // increment pointer 
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}

void strcopy(char * dest, const char * source)
{
	// Start the source pointer at the beginning
	while(*source)
	{
		*dest = *source; // Copy the source character to the desination
		dest++; // increment pointer
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}