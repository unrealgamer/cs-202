/** 
*	Shane Melton
*	CS 202 Lab/Studio - 1104
*	September 4, 2014
**/
#include <iostream>

using namespace std;

int main(int argc, char * argv[])
{
	if(argc > 1)
	{
		for(int i = 1; i < argc; i++)
		{
			cout << argv[i] << " ";
		}
		cout << endl;
	}
	else
	{
		cout << "Please provide at least one argument" << endl;
		return 1;
	}
	return 0;
}