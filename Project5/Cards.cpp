/********************************************************************************/
/* PROGRAM: Project 5 Card Shuffler / Dealer (Dynamic)                  		*/
/*																				*/
/* AUTHOR:  Shane Melton														*/
/* 																				*/
/* DATE:    September 21, 2014 													*/
/*                                                                              */
/* REVISIONS: 1                                                                	*/
/*                                                                              */
/* PURPOSE: A small program that will read a deck of 52 cards and shuffle them. */
/*			It also will deal out the cards in a round robin fashion.			*/
/*                                                                              */
/********************************************************************************/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

#include "CStrings.h"

// Constants for the number of cards in a deck, number of players

const int CARDS = 52;
const int HAND_LENGTH = 3;



// A struct that represents a custom Card datatype
struct Card
{
	char 	*suit;
	char 	*rank;
	int 	cvalue;
	char 	*location;
};

// A struct that represents a custom Player datatype
struct Player
{
	char 	*name;
	Card 	*hand;
	int 	total;
};

// Function Prototypes

/**
	Prints the menu to the console. (Does not wait for any user input)
*/
void printMenu();

/**
	Loads a deck of cards from a file and stores them into the single dimensiaonal array provided
	@param Card* The array to be filled with cards
*/
bool loadUnshuffledDeck(Card *);

/**
	Loads a list of players from a file and stores them into the single dimensiaonal array provided
	@param Player* The array to be filled with players
	@return The number of players loaded. (-1 if load failed)
*/
int loadPlayers(Player **);

/**
	Initializes the deck (array) by filling it with a default card value
	@param Card* The deck to reset
	@param deckLength The length of the provided deck
*/
void initDeck(Card *, int deckLength = CARDS);

/**
	Initializes the player hand (array) by filling it with a default card value
	@param Player* The player to reset
*/
void initPlayerHand(Player *);

/**
	Takes cards from the unshuffled deck (which never changes after load) and randomly orders
	them within the shuffled deck.
	@param shuffledDeck* The deck to be shuffled
	@param unShuffledDeck* The static deck used to populate the shuffled deck
*/
bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck);

/**
	Deals the cards from the shuffledDeck to the players and then one card to the discard and the rest
	are dealt into the stock pile.
	@param shuffledDeck The deck of shuffled cards
	@param numPlayers The number of players to deal to
	@param players The players to deal to
	@param discard The deck to deal discarded cards to
	@param stockPile The deck to deal leftover cards to
*/
void dealCards(Card * shuffledDeck, int numPlayers, Player * players, Card ** discard, Card ** stockPile);

/**
	Prints a card and all of its attributes to the console
	@param Card The card to print
*/
void printCard(Card);

/**
	Prints all of the players to screen including their hand and other attributes
	@param Player* The array of players
*/
void printPlayers(Player *, int);

/**
	Prints every deck to screen
	@param Self explanatory
*/
void printAllDecks(Card * shuffledDeck, Card * unShuffledDeck, Card * discard, Card * stockPile);

/**
	Does a deep copy of the source card into the destination. Also the card has its location set
	to the provided new location C string.
	@param dest The destination card
	@param source The source card
	@param newLocation The new location for the card (typically the destination deck)
*/
void cardCopy(Card * dest, Card * source, const char *  newLocation);

/**
	Allocates exactly enough memory for a new C style string and copies the contents of the provided C string
	into the newly created array.
	@param The C string to copy into the new dynmaic C String
	@return The pointer to the newly created string
*/
char * createDynamicString(char const *);

/**
	Instantiates the provided card pointer with default values
	@param The card to populate with default values
*/
void initDefaultCard(Card *);

/**
	Deletes the C strings inside of the provided card and frees their memory
	@param The card to clear
*/
void deleteCardValues(Card *);

/**
	Deletes the C strings and hand inside of the provided player and frees their memory
	@param The player to clear
*/
void deletePlayerValues(Player *);

/**
	Iterates through the provided deck and clears the memory for each struct
*/
void deleteDeck(Card *, int deckLength = CARDS);

/**
	Iterates throught the provided players and clears the allocated memory associated with them
*/	
void deletePlayers(Player *, int);

// End Function Prototypes

using namespace std;

// Main entry point of the program
int main() {

	// Integer used to store the users selection in the menus
	int userSel = 0;

	// Booleans to check if the deck and players were loaded succesfully
	bool loadedDeck = false;
	bool deckShuffled = false;
	int numPlayers = -1;

	// Array for deck and players
	Card * unShuffledDeck = new Card[CARDS];
	Card * shuffledDeck = new Card[CARDS];

	Card * discardDeck = NULL;
	Card * stockPileDeck = NULL;

	// Sets every card in each deck to the default card
	initDeck(unShuffledDeck);
	initDeck(shuffledDeck);

	// Set the player array to null since it is created at runtime
	Player * thePlayers = NULL;

	while(userSel != 7)
	{
		printMenu(); // Print the menu to the screen

		cin >> userSel; // Wait for the user's input

		if(!cin.good()) // Verify the user's input is an integer
		{
			cout << "Improper Selection! Please try again!" << endl;

			cin.clear(); // Clear the input buffer

			cin.ignore(); // Flush the new line character

			continue;
		}

		cin.ignore(); // Flush the new line character '\n' from the buffer

		switch(userSel)
		{
			case 1: // Load Deck File
			{
				loadedDeck = loadUnshuffledDeck(unShuffledDeck);
				cout << endl;
				break;
			}
			case 2: // Load Player File
			{
				numPlayers = loadPlayers(&thePlayers);
				cout << endl;
				break;
			}
			case 3: // Shuffles the deck
			{
				if(loadedDeck)
				{
					deckShuffled = shuffleDeck(shuffledDeck, unShuffledDeck);
					cout << "Deck Shuffled Succesfully!" << endl;
				}
				else
					cout << "Please load the deck first. (Option 1)" << endl;
				break; 
			}
			case 4:
			{
				if(deckShuffled && numPlayers != -1)
				{
					dealCards(shuffledDeck, numPlayers, thePlayers, &discardDeck, &stockPileDeck);
				}
				else
					cout << "Please shuffle the deck first or load the players. (Options 2 & 3)" << endl;
				break;
			}
			case 5: // Print Players and Hands
			{
				if(loadedDeck && numPlayers != -1)
					printPlayers(thePlayers, numPlayers);
				else
					cout << "Please load the deck and players first. (Option 1 & 2)" << endl;
				break;
			}
			case 6: // Print all decks to screen
			{
				if(loadedDeck && loadPlayers)
					printAllDecks(shuffledDeck, unShuffledDeck, discardDeck, stockPileDeck); 
				else
					cout << "Please make sure everything is loaded and atleast one shuffle has occured." << endl;
				break;
			}
			case 7: // Exit the program
			{
				cout << "Good Bye" << endl;
				//Free up all allocated memory
				deleteDeck(unShuffledDeck);
				deleteDeck(shuffledDeck);
				deleteDeck(discardDeck);
				deleteDeck(stockPileDeck);
				deletePlayers(thePlayers, numPlayers);
				return 0;
			}
			default: // Out of range option
			{
				cout << "Please select an option 1-6." << endl;
				break;
			}
		}
		cout << "Press the enter key to continue...";
		cin.ignore();
	}
	// Clear up all memory if the program exited improperly
	deleteDeck(unShuffledDeck);
	deleteDeck(shuffledDeck);
	deleteDeck(discardDeck);
	deleteDeck(stockPileDeck);
	deletePlayers(thePlayers, numPlayers);

	return 0;
}


void printMenu()
{
	cout << endl << endl << "Select an option below: " << endl;

	cout << "1. Load Card File" << endl;
	cout << "2. Load Player File" << endl;
	cout << "3. Shuffle Deck" << endl;
	cout << "4. Deal Cards" << endl;
	cout << "5. Print Players" << endl;
	cout << "6. Print All Decks" << endl;
	cout << "7. Exit" << endl;

	cout << "Selection: ";
}

bool loadUnshuffledDeck(Card * unShuffledDeck)
{
	//Ask the user for a file name and store it in a C String
	char * temp = new char[50];

	cout << "Enter the input file name: ";

	cin.getline(temp, 50);

	// Create a dynamic string for the file name
	char * fileName = createDynamicString(temp);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	Card * cptr = unShuffledDeck;

	// Loop through each card and increment the count and pointer
	for(int i = 0; i < CARDS; cptr++, i++)
	{
		// Clear out any default values in the Card
		delete [] (*cptr).suit; (*cptr).suit = NULL;
		delete [] (*cptr).rank;	(*cptr).rank = NULL;
		delete [] (*cptr).location; (*cptr).location = NULL;

		// Read in each element of the card and place it in the temp array so it can be created dynamically
		ifs >> temp;
		(*cptr).suit = createDynamicString(temp);
		ifs >> temp;
		(*cptr).rank = createDynamicString(temp);
		ifs >> (*cptr).cvalue;
		(*cptr).location = createDynamicString("unshuffled");
	}

	//Cleanup
	ifs.close();
	delete [] fileName;
	delete [] temp;

	cout << "File Succesfully Read! ";
	return true; //Success
}

int loadPlayers(Player ** thePlayers)
{
	//Ask the user for a file name and store it in a temp C String
	char * temp = new char[50];

	cout << "Enter the input file name: ";

	cin.getline(temp, 50);

	// Create a dymnamic string for the fileName
	char * fileName = createDynamicString(temp);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	int numPlayers = -1;

	cout << "How many players would like to play? (2-8): ";

	cin >> numPlayers;

	(*thePlayers) = new Player[numPlayers];

	Player * pptr = (*thePlayers);

	char * tempLast = new char[50];

	// Loop through each player and increment the count and pointer
	for(int i = 0; i < numPlayers; pptr++, i++)
	{
		ifs >> temp;
		ifs >> tempLast;
		strcat(temp, " ");
		strcat(temp, tempLast);
		(*pptr).name = createDynamicString(temp);
		(*pptr).total = 0;
		initPlayerHand(pptr);
	}

	//Clean up
	ifs.close();
	delete [] temp;
	delete [] tempLast;
	delete [] fileName;

	cout << "File Succesfully Read! ";
	return numPlayers; //Success
}

void initDeck(Card * deck, int deckLength)
{
	// Card Pointer for iterating through the deck
	Card * cptr = deck;

	// Loop through and increment pointer and counter
	for(int i = 0; i < deckLength; cptr++, i++)
	{
		// Set default values
		initDefaultCard(cptr);
	}
}

void initPlayerHand(Player * player)
{
	(*player).hand = new Card[HAND_LENGTH];

	Card * cptr = (*player).hand;

	for(int i = 0; i < HAND_LENGTH; i++, cptr++)
	{
		initDefaultCard(cptr);
	}
}

bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck)
{
	// Create pointers for the shuffled and unshuffled decks
	Card * shuPtr = shuffledDeck;
	Card * unsPtr = unShuffledDeck;

	// Deep copy unShuffledDeck to shuffleDeck (for loop increments pointers and counter)
	for(int i = 0; i < CARDS; i++, shuPtr++, unsPtr++)
		cardCopy(shuPtr, unsPtr, "location");

	// Seed
	srand(time(0));

	// Reset Shuffled Pointer
	shuPtr = shuffledDeck;

	// Temp card used for swapping
	Card * temp = new Card;
	initDefaultCard(temp);	

	// Loop through each card and shuffle it with a random index to swap ("shuffle") the cards
	for(int i = 0; i < CARDS; shuPtr++, i++)
	{
		int j = rand()%CARDS; 						// Random index to shuffle
		Card * otherPtr = shuffledDeck; 			// Other Card pointer used for swapping
		for(int k = 0; k < j; k++, otherPtr++);	// Increment otherPtr to the random index

		// If the indices are the same don't try and swapp
		if ( i == j)
			continue;
		cardCopy(temp, otherPtr, "shuffled");		// Perform the swap
		cardCopy(otherPtr, shuPtr, "shuffled");
		cardCopy(shuPtr, temp, "shuffled");
	}

	// Clean up
	deleteCardValues(temp);
	delete temp;
	return true;
}

void dealCards(Card * shuffledDeck, int numPlayers, Player * players, Card ** discard, Card ** stockPile)
{
	
	int maxCardsToDeal= numPlayers * HAND_LENGTH;

	int curCard = 0;

	Card * cptr;
	Card * shuPtr = shuffledDeck;
	Player * pptr;

	while(curCard < maxCardsToDeal)
	{
		// Go back to the first player once we get to the end
		if(curCard % numPlayers == 0) 
			pptr = players;

		// Get the pointer to the current player's hand
		cptr = (*pptr).hand; 
		
		// Move to the current hand position
		for(int i = 0; i < curCard / numPlayers; i++, cptr++); 

		// Copy the next shuffled card into the player's hand
		cardCopy(cptr, shuPtr, (*pptr).name);

		// Increment the total of the players hand
		(*pptr).total += (*cptr).cvalue;

		// Increment all pointers
		shuPtr++;
		curCard++;
		pptr++;
	}

	// Create a new discard deck
	(*discard) = new Card[CARDS];
	initDeck((*discard));

	// Copy the next card into the discard deck
	cardCopy((*discard), shuPtr, "discard");

	// Increment the shuffledDeck pointer
	shuPtr++;

	//Increment the curCard counter
	curCard++;

	// Create a new stockPile deck
	(*stockPile) = new Card[CARDS];
	initDeck((*stockPile));

	// Create a pointer to the stockPile deck
	Card * stockPtr = (*stockPile);

	//Increment through the rest of the shuffled cards and copy them to stockPile deck
	for(int i = curCard; i < CARDS; i++, stockPtr++, shuPtr++)
	{
		cardCopy(stockPtr, shuPtr, "stockPile");
	}
}

void printPlayers(Player * thePlayers, int numPlayers)
{
	// Player pointer for interating through the deck
	Player * pPtr = thePlayers;
	Card * cptr = NULL;
	for(int i = 0; i < numPlayers; i++, pPtr++)
	{
		cout << "Player: " << (*pPtr).name << "  Total: " << (*pPtr).total << endl;

		cout << "Hand: " << endl;

		cptr = (*pPtr).hand;
		for(int j = 0; j < HAND_LENGTH; j++, cptr++)
		{
			cout << "\t"; printCard((*cptr)); cout << endl;
		}
	}
}

void printAllDecks(Card * shuffledDeck, Card * unShuffledDeck, Card * discard, Card * stockPile)
{
	// Card Pointer used for iterating through the card arrays
	Card * cPtr = shuffledDeck;

	cout << "The shuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		cout << '\t';
		printCard((*cPtr)); // Print the card
		cout << endl;
	}

	// Set the pointer to the other array
	cPtr = unShuffledDeck;

	cout << "The unShuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		cout << '\t';
		printCard((*cPtr)); // Print the card
		cout << endl;
	}

	cPtr = discard;

	cout << "The dicard Deck: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		if(strcomp((*cPtr).location, "discard"))
		{
			cout << '\t';
			printCard((*cPtr));
			cout << endl;
		}
	}

	cPtr = stockPile;

	cout << "The Stock Pile: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		if(strcomp((*cPtr).location, "stockPile"))
		{
			cout << '\t';
			printCard((*cPtr));
			cout << endl;
		}	
	}
}

void printCard(Card card)
{
	cout << card.suit << " " << card.rank;
	cout << " (" << card.cvalue << ") Location: " << card.location;
}

void cardCopy(Card * dest, Card * source, const char * newLocation)
{
	// Delete and free up any and all old values
	delete [] (*dest).suit; (*dest).suit = NULL;
	delete [] (*dest).rank; (*dest).rank = NULL;
	delete [] (*dest).location; (*dest).location = NULL;
	delete [] (*source).location; (*source).location = NULL;

	//Copy over the new values
	(*dest).suit = createDynamicString((*source).suit);
	(*dest).rank = createDynamicString((*source).rank);
	(*dest).location = createDynamicString(newLocation);
	(*source).location = createDynamicString(newLocation);

	(*dest).cvalue = (*source).cvalue;
}

char * createDynamicString(char const * source)
{
	char * ret = new char[strlen(source) + 1];
	strcopy(ret, source);
	return ret;
}

void initDefaultCard(Card * card)
{
	(*card).suit = createDynamicString("suit");
	(*card).rank = createDynamicString("rank");
	(*card).location = createDynamicString("location");
	(*card).cvalue = 0;
}

void deleteCardValues(Card * card)
{
	delete [] (*card).suit; (*card).suit = NULL;
	delete [] (*card).rank;	(*card).rank = NULL;
	delete [] (*card).location; (*card).location = NULL;
}

void deletePlayerValues(Player * player)
{
	delete [] (*player).name; (*player).name = NULL;
	deleteDeck((*player).hand, HAND_LENGTH);
}

void deleteDeck(Card * deck, int deckLength)
{
	if(deck == NULL)
		return;
	Card * card = deck;
	for(int i = 0; i < deckLength; i++, card++)
		deleteCardValues(card);
	delete [] deck; deck = NULL;
}

void deletePlayers(Player * thePlayers, int numPlayers)
{
	Player * player = thePlayers;
	for(int i = 0; i < numPlayers; i++, player++)
		deletePlayerValues(player);
	delete [] thePlayers;
}