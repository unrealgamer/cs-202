#include "CStrings.h"

void strcat(char * dest, const char * source)
{
	// Move the destination pointer to the end of the C string
	while(*dest) // '\0' character will evalulate to false
		dest++; 
	// Start the source at the beginning 
	while(*source)
	{
		*dest = *source; // Copy the source character to the destination
		dest++; // increment pointer 
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}

void strcopy(char * dest, const char * source)
{
	// Start the source pointer at the beginning
	while(*source)
	{
		*dest = *source; // Copy the source character to the desination
		dest++; // increment pointer
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}

int strlen(char const * str)
{
	int length = 0;
	while(*str++)
		length++;
	return length;
}

bool strcomp(const char * one, const char * two)
{
	while(*one != '\0' && *two != '\0')
	{
		if(*one != *two)
			return false;
		one++;
		two++;
	}

	if(*one != *two)
		return false;

	return true;
}