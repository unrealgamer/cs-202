//Stack1 (Stay)

#include "stack.h"

/**
*	Default Constructor
*	@param length Max length of the stack (default is 10)
*/
Stack::Stack(int length) 
: max(length)
, top(0)
, actual(0)
{
	this->data = new char[max];
}

/**
*	Copy Constructor
*	@param stack The stack to copy
*/
Stack::Stack(const Stack& stack)
{
	(*this) = stack;
}

/**
*	Default Deconstructor
*/
Stack::~Stack()
{
	// If the data isn't null delete it
	if(this->data != 0)
		delete [] this->data;
}

/**
*	Assignment Operator
*	@param rhs The value to be assigned
*/
Stack& Stack::operator=(const Stack& rhs)
{
	// Return this if self assignment
	if(&rhs == this)
		return *this;

	// Assign Members
	this->max = rhs.max;
	this->top = rhs.top;
	this->actual = rhs.actual;

	// Delete this data if it is not null
	if(this->data != 0)
		delete [] data;

	// Create new data with the newly assigned max
	this->data = new char[this->max];

	// Copy the values from the new data to this data
	for(int i = 0; i <= actual-1; i++)
		this->data[i] = rhs.data[i];

	return *this;
}

/**
*	Pushes the provided char to the top of the stack
*	@param c The character to stack
*	@return True on success, False on failure
*/
bool Stack::push(char c)
{
	// Don't stack if full
	if(full())
		return false;

	// Shuffle all elements down
	for(int i = actual-1; i >= top; i--)
	{
		this->data[i+1] = this->data[i];
	}

	// Assign the top element to the parameter
	this->data[top] = c;
	// Increment the count
	this->actual++;

	return true;
}

/**
*	Pops the top element in the stack and assigns it to the provided parameter
*	@param c The reference to be assigned the dequeued value
*	@return True on success, False on failure
*/
bool Stack::pop(char & c)
{
	// Don't pop if empty
	if(empty())
		return false;

	// Assign the parameter to the popped element
	c = this->data[top];

	// Shuffle all elements up
	for(int i = top; i < actual-1; i++)
	{
		this->data[i] = this->data[i+1];
	}

	// Decrement the count
	this->actual--;

	return true;
}

/**
*	Checks if the stack is empty
*	@return True if empty, False if not
*/
bool Stack::empty() const
{
	// If the acuta is zero then empty
	return (this->actual == 0);
}

/**
*	Checks if the stack is full
*	@return True if full, False if not
*/
bool Stack::full() const
{
	// If the actual is the max then full
	return (this->actual == this->max);
}

/**
*	Clears thhe count and the top pointer
* 	@return True on success
*/
bool Stack::clear()
{

	this->top = 0;
	this->actual = 0;

	return true;
}

/**
*	Comparison Operator (size and values)
*	@param rhs The Stack to compare
*	@return True if the stacks are functionaly identical, False if not
*/
bool Stack::operator==(const Stack& rhs) const
{
	//Do they have the same number of elements
	if(rhs.actual != this->actual || rhs.max != this->max)
		return false;

	// Are the values equal
	for(int i = 0; i <= this->actual-1; i++)
		if(rhs.data[i] != this->data[i])
			return false;

	return true;
}

/**
*	Insertion Operator (Prints the stack in order)
*	@param out The stream to write to
*	@param rhs The stack to print out
* 	@return The output stream that was written to
*/
ostream& operator<<(ostream& out, const Stack& rhs)
{
	// Print empty if empty
	if(rhs.actual == 0)
	{
		out << "Empty!";
	}
	else	
	{
		// Cycle through each element (except last) and print
		for(int i = 0; i <= rhs.actual-2; i++)
		{
			out << rhs.data[i] << ", ";
		}
		// Print the last without the comma
		out << rhs.data[rhs.actual-1];
	}

	// Print out the size
	out << "\t\t\t\tSize: " << rhs.actual << "/" << rhs.max;

	return out;
}