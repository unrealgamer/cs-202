//Queue2 (Wrap)

#include "queue.h"

/**
*	Default Constructor
*	@param length Max length of the queue (default is 10)
*/
Queue::Queue(int length) 
: max(length)
, front(-1)
, rear(-1)
{
	this->data = new int[max];
}

/**
*	Copy Constructor
*	@param queue The queue to copy
*/
Queue::Queue(const Queue& queue)
{
	(*this) = queue;
}

/**
*	Default Deconstructor
*/
Queue::~Queue()
{
	// If the data isn't null delete it
	if(this->data != 0)
		delete [] this->data;
}

/**
*	Assignment Operator
*	@param rhs The value to be assigned
*/
Queue& Queue::operator=(const Queue& rhs)
{
	// Return this if self assignment
	if(&rhs == this)
		return *this;

	// Assign Members
	this->max = rhs.max;
	this->front = rhs.front;
	this->rear = rhs.rear;

	// Delete this data if it is not null
	if(this->data != 0)
		delete [] data;

	// Create new data with the newly assigned max
	this->data = new int[this->max];

	// Copy the values from the new data to this data
	for(int i = 0; i <= max; i++)
		this->data[i] = rhs.data[i];

	return *this;
}

/**
*	Queues up the provided integer at the end of the queue
*	@param n The integer to queue
*	@return True on success, False on failure
*/
bool Queue::enqueue(int n)
{
	// Stop enqueuing if full
	if(full())
		return false;

	// If empty reset the pointers for new elements
	if(empty())
	{
		this->front = 0;
		this->rear = 0;
	}

	// Queue the new element at the rear
	this->data[this->rear] = n;

	// Increment the rear (and wrap)
	this->rear++;
	this->rear = this->rear % this->max;

	return true;
}

/**
*	Dequeues the first element in the queue and assigns it to the provided parameter
*	@param n The reference to be assigned the dequeued value
*	@return True on success, False on failure
*/
bool Queue::dequeue(int & n)
{
	// Don't dequeue if the queue is empty
	if(empty())
		return false;

	// Assign the parameter to equal the first element
	n = this->data[this->front];

	// Increment front and wrap
	this->front++;
	this->front = this->front % this->max;

	// Clear the queue if we emptied the queue
	if(this->front == this->rear)
		clear();

	return true;
}

/**
*	Checks if the queue is empty
*	@return True if empty, False if not
*/
bool Queue::empty() const
{
	// If the rear equals front and rear is invalid then empty
	return (this->rear == this->front && this->rear < 0);
}

/**
*	Checks if the queue is full
*	@return True if full, False if not
*/
bool Queue::full() const
{
	// If the rear equals the front and rear is valid then full
	return (this->rear == this->front && this->rear >= 0);
}

/**
*	Clears the front and rear pointers
* 	@return True on success
*/
bool Queue::clear()
{

	// Reset the rear pointer to be out of bounds
	this->front = -1;
	this->rear 	= -1;

	return true;
}

/**
*	Comparison Operator (size, positions, and values)
*	@param rhs The Queue to compare
*	@return True if the queues are functionaly identical, False if not
*/
bool Queue::operator==(const Queue& rhs) const
{
	// If all members are not equal return false
	if(rhs.front != this->front || rhs.rear != this->rear || rhs.max != this->max)
		return false;

	// Is each element equal?
	for(int i = this->front; i < this->rear; i++)
		if(rhs.data[i] != this->data[i])
			return false;

	return true;
}


/**
*	Insertion Operator (Prints the queue in order)
*	@param out The stream to write to
*	@param rhs The queue to print out
* 	@return The output stream that was written to
*/
ostream& operator<<(ostream& out, const Queue& rhs)
{
	// Print empty if empty
	if(rhs.empty())
	{
		out << "Empty!";
	}
	else	
	{
		// Cycle through each element (except last) and print
		for(int i = rhs.front; i < ((rhs.rear == 0) ? rhs.max-1 : rhs.rear-1); i++, i = i % rhs.max)
		{
			out << rhs.data[i] << ", ";
		}
		// Print the last element without the comma
		out << rhs.data[((rhs.rear == 0) ? rhs.max-1 : rhs.rear-1)];
	}


	return out;
}