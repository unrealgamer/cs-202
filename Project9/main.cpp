#include <iostream>

#include "stack.h"
#include "queue.h"

using namespace std;

int main()
{

	Queue * q = new Queue(10);

	int t;

	q->enqueue(1);
	cout << *q << endl;
	q->enqueue(2);
	cout << *q << endl;
	q->enqueue(3);
	cout << *q << endl;
	q->enqueue(4);

	cout << *q << endl;

	// Queue * q2 = new Queue(5);

	// cout << "Q2" << endl;

	// cout << *q2 << endl;

	// q2->enqueue(1);
	// q2->enqueue(2);
	// q2->enqueue(3);
	// q2->enqueue(4);

	// cout << "Q1 == Q2 : " << (*q2 == *q) << endl;

	// q->dequeue(t);
	// q->clear();

	// cout << "Q1 == Q2 : " << (*q2 == *q) << endl;

	// cout << *q2 << endl;

	// cout << "Q1" << endl << *q << endl;



	q->dequeue(t);
	q->dequeue(t);
	q->dequeue(t);
	q->dequeue(t);

	//cout << t << endl;
	
	cout << *q << endl;

	q->enqueue(5);
	q->enqueue(6);
	q->dequeue(t);
	q->enqueue(7);
	cout << "T: " << t << endl;
	cout << *q << endl;

	delete q;



	// Stack * s = new Stack();

	// s->push('a');
	// s->push('b');
	// s->push('c');

	// cout << *s << endl;

	// char pop;

	// s->pop(pop);
	// cout << "Pop: " << pop << endl;
	// cout << *s << endl;
	// s->pop(pop);
	// cout << "Pop: " << pop << endl;
	// cout << *s << endl;
	// s->pop(pop);

	// cout << "Pop: " << pop << endl;

	// cout << *s << endl;

	// delete s;

	return 0;
}