//Stack2 (move)

#include "stack.h"

/**
*	Default Constructor
*	@param length Max length of the stack (default is 10)
*/
Stack::Stack(int length) 
: max(length)
, top(0)
, actual(0)
{
	this->data = new char[max];
}

/**
*	Copy Constructor
*	@param stack The stack to copy
*/
Stack::Stack(const Stack& stack)
{
	(*this) = stack;
}

/**
*	Default Deconstructor
*/
Stack::~Stack()
{
	if(this->data != 0)
		delete [] this->data;
}

/**
*	Assignment Operator
*	@param rhs The value to be assigned
*/
Stack& Stack::operator=(const Stack& rhs)
{
	// Return this if self assignment
	if(&rhs == this)
		return *this;

	// Assign members
	this->max = rhs.max;
	this->top = rhs.top;
	this->actual = rhs.actual;

	// Delete this data if it's not null
	if(this->data != 0)
		delete [] data;

	// Create new data with newly assigned max
	this->data = new char[this->max];

	// Copy the values from the new data to this data
	for(int i = 0; i <= actual-1; i++)
		this->data[i] = rhs.data[i];

	return *this;
}

/**
*	Pushes the provided char to the top of the stack
*	@param c The character to stack
*	@return True on success, False on failure
*/
bool Stack::push(char c)
{
	// Don't push if full
	if(full())
		return false;

	// Add new character to top of the stack
	this->data[this->top] = c;

	// Increment the top pointer
	this->top++;

	return true;
}

/**
*	Pops the top element in the stack and assigns it to the provided parameter
*	@param c The reference to be assigned the dequeued value
*	@return True on success, False on failure
*/
bool Stack::pop(char & c)
{
	// Don't pop if empty
	if(empty())
		return false;

	// Set the parameter to the top element
	c = this->data[this->top-1];

	// Decrement the top pointer
	this->top--;

	return true;
}

/**
*	Checks if the stack is empty
*	@return True if empty, False if not
*/
bool Stack::empty() const
{
	// If top is at begining then empty
	return (this->top == 0);
}

/**
*	Checks if the stack is full
*	@return True if full, False if not
*/
bool Stack::full() const
{
	//If the top is at the end then full
	return (this->top == this->max);
}

/**
*	Clears thhe count and the top pointer
* 	@return True on success
*/
bool Stack::clear()
{
	this->top = 0;

	return true;
}

/**
*	Comparison Operator (size and values)
*	@param rhs The Stack to compare
*	@return True if the stacks are functionaly identical, False if not
*/
bool Stack::operator==(const Stack& rhs) const
{
	// Are the sizes the same
	if(rhs.actual != this->actual || rhs.max != this->max)
		return false;

	// Are the values equal
	for(int i = 0; i <= this->actual-1; i++)
		if(rhs.data[i] != this->data[i])
			return false;

	return true;
}

/**
*	Insertion Operator (Prints the stack in order)
*	@param out The stream to write to
*	@param rhs The stack to print out
* 	@return The output stream that was written to
*/
ostream& operator<<(ostream& out, const Stack& rhs)
{
	// Print empty if empty
	if(rhs.empty())
	{
		out << "Empty!";
	}
	else	
	{
		// Cycle through the elements in order
		for(int i = rhs.top; i <= 1; i++)
		{
			out << rhs.data[i] << ", ";
		}
		// Print out last element without comma
		out << rhs.data[0];
	}

	// Print out the size
	out << "\t\t\t\tSize: " << rhs.top << "/" << rhs.max;

	return out;
}