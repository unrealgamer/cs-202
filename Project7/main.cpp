#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>


#include "CardPlayer.h"
#include "CStrings.h"

using namespace std;

const int CARDS = 52;
const int HAND_LENGTH = 3;

/**
	Prints the menu to the console. (Does not wait for any user input)
*/
void printMenu();

/**
	Loads a deck of cards from a file and stores them into the single dimensiaonal array provided
	@param Card* The array to be filled with cards
*/
bool loadUnshuffledDeck(Card *);

/**
	Loads a list of players from a file and stores them into the single dimensiaonal array provided
	@param Player* The array to be filled with players
	@return The number of players loaded. (-1 if load failed)
*/
int loadPlayers(Player **);

/**
	Takes cards from the unshuffled deck (which never changes after load) and randomly orders
	them within the shuffled deck.
	@param shuffledDeck* The deck to be shuffled
	@param unShuffledDeck* The static deck used to populate the shuffled deck
*/
bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck);

/**
	Prints each card in the provided deck in order on a seperate line
	@param deck* The deck to print
	@param length The number of cards in the deck to print
*/
void printDeck(Card * deck, int length);

/**
	Prints each player in the provided list of players
	@param players* The list of players to print
	@param numPlayers The number of players to print
*/
void printPlayers(Player * players, int numPlayers);

/**
	Main Entry point of the program
*/
int main()
{

	// User selection
	int userSel = 0;

	// Booleans to track curretnt state of the application
	bool deckLoaded = false;
	bool deckShuffled = false;

	// Number of players in the game
	int numPlayers = -1;

	// The decks to be used throughout
	Card * unShuffledDeck = new Card[CARDS];
	Card * shuffledDeck = new Card[CARDS];

	Card * discardDeck = NULL;
	Card * stockPileDeck = NULL;

	// The list of players
	Player * thePlayers = NULL;

	while(userSel != 7)
	{
		// Print the menu
		printMenu();

		// Get the input from the user
		cin >> userSel;

		if(!cin.good()) // Verify the user's input is an integer
		{
			cout << "Improper Selection! Please try again!" << endl;

			cin.clear(); // Clear the input buffer

			cin.ignore(); // Flush the new line character

			continue;
		}

		cin.ignore(); // Flush the new line character '\n' from the buffer

		switch(userSel)
		{
			case 1: // Load Deck File
			{
				deckLoaded = loadUnshuffledDeck(unShuffledDeck);
				cout << endl;
				break;
			}
			case 2: // Load Player File
			{
				numPlayers = loadPlayers(&thePlayers);
				cout << endl;
				break;
			}
			case 3: // Shuffles the deck
			{
				if(deckLoaded)
				{
					deckShuffled = shuffleDeck(shuffledDeck, unShuffledDeck);
					cout << "Deck Shuffled Succesfully!" << endl;
				}
				else
					cout << "Please load the deck first. (Option 1)" << endl;
				break; 
			}
			case 4: // Deal the cards following skat rules
			{
				// The deck was shuffled and players loaded
				if(deckShuffled && numPlayers != -1)
				{
					// Indices and totals to keep track of the current deal
					int maxCardToDeal = numPlayers * HAND_LENGTH;
					int curCard = 0;
					int curPlayer = 0;

					// While we haven't dealt enough cards, keep dealing
					while(curCard < maxCardToDeal)
					{
						// If we get to the end of a round robin deal, go back to the first player
						if(curCard % numPlayers == 0)
							curPlayer = 0;

						// Copy (deal) the card from the shuffledDeck to the player and change the location
						thePlayers[curPlayer].copyCardAt(curCard/numPlayers, shuffledDeck[curCard]);
						shuffledDeck[curCard].setLocation(thePlayers[curPlayer].getName());

						curCard++;
						curPlayer++;
					}

					// Create the discard deck and copy the next card into it
					discardDeck = new Card[1];
					(*discardDeck).cardCopy(&shuffledDeck[curCard], "discard");
					curCard++;

					// The size of the stockPile deck is the number of cards we have left
					int stockSize = CARDS - curCard;

					// Create the stockPile deck
					stockPileDeck = new Card[stockSize];

					// Index for the stock deck
					int curStock = 0;

					while(curStock < stockSize)
					{
						// Copy (deal) the card from the shuffled deckto the stockPile deck
						stockPileDeck[curStock].cardCopy(&shuffledDeck[curCard], "stockPile");
						curStock++;
						curCard++;
					}

				}
				else
					cout << "Please shuffle the deck first or load the players. (Options 2 & 3)" << endl;
				break;
			}
			case 5: // Print Players and Hands
			{
				if(deckLoaded && numPlayers != -1)
					printPlayers(thePlayers, numPlayers);
				else
					cout << "Please load the deck and players first. (Option 1 & 2)" << endl;
				break;
			}
			case 6: // Print all decks to screen
			{
				if(deckLoaded)// && numPlayers != -1)
				{
					cout << "\nunShuffledDeck: " << endl;
					printDeck(unShuffledDeck, CARDS);
					cout << "\nshuffledDeck: " << endl;
					printDeck(shuffledDeck, CARDS);
					cout << "\ndiscardDeck: " << endl;
					printDeck(discardDeck, 1);
					cout << "\nstockPileDeck: " << endl;
					printDeck(stockPileDeck, CARDS - (numPlayers*HAND_LENGTH) - 1);
				}
				else
					cout << "Please make sure everything is loaded and atleast one shuffle has occured." << endl;
				break;
			}
			case 7: // Exit the program
			{
				cout << "Good Bye" << endl;
				break;
			}
			default: // Out of range option
			{
				cout << "Please select an option 1-6." << endl;
				break;
			}
		}
	}

	delete [] unShuffledDeck;
	delete [] shuffledDeck;
	delete [] discardDeck;
	delete [] stockPileDeck;
	delete [] thePlayers;

	return 0;
}

void printMenu()
{
	cout << endl << endl << "Select an option below: " << endl;

	cout << "1. Load Card File" << endl;
	cout << "2. Load Player File" << endl;
	cout << "3. Shuffle Deck" << endl;
	cout << "4. Deal Cards" << endl;
	cout << "5. Print Players" << endl;
	cout << "6. Print All Decks" << endl;
	cout << "7. Exit" << endl;

	cout << "Selection: ";
}

bool loadUnshuffledDeck(Card * unShuffledDeck)
{
	//Ask the user for a file name and store it in a C String
	char * temp = new char[50];

	cout << "Enter the input file name: ";

	cin.getline(temp, 50);

	// Create a file stream using the fileName
	ifstream ifs (temp);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << temp << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	int tempV = 0;

	// Loop through each card and increment the count 
	for(int i = 0; i < CARDS; i++)
	{
		// Read each element into the temp and place it in the card object
		ifs >> temp;
		unShuffledDeck[i].setSuit(temp);
		ifs >> temp;
		unShuffledDeck[i].setRank(temp);

		ifs >> tempV;
		unShuffledDeck[i].setValue(tempV);

		unShuffledDeck[i].setLocation("unshuffled");
	}

	//Cleanup
	ifs.close();
	delete [] temp;

	cout << "File Succesfully Read! ";
	return true; //Success
}

int loadPlayers(Player ** thePlayers)
{
	//Ask the user for a file name and store it in a temp C String
	char * temp = new char[50];

	cout << "Enter the input file name: ";

	cin.getline(temp, 50);

	// Create a file stream using the fileName
	ifstream ifs (temp);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << temp << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	int numPlayers = -1;

	cout << "How many players would like to play? (2-8): ";

	cin >> numPlayers;

	(*thePlayers) = new Player[numPlayers];

	char * tempLast = new char[50];

	// Loop through each player and increment the counter
	for(int i = 0; i < numPlayers;i++)
	{
		ifs >> temp;
		ifs >> tempLast;
		strcat(temp, " ");
		strcat(temp, tempLast);
		(*thePlayers)[i].setName(temp);
	}

	//Clean up
	ifs.close();
	delete [] temp;
	delete [] tempLast;

	cout << "File Succesfully Read! ";
	return numPlayers; //Success
}

bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck)
{

	// Deep copy unShuffledDeck to shuffleDeck (for loop increments pointers and counter)
	for(int i = 0; i < CARDS; i++)
		shuffledDeck[i].cardCopy(&unShuffledDeck[i], "location");

	// Seed
	srand(time(0));

	// Temp card used for swapping
	Card * temp = new Card;

	// Loop through each card and shuffle it with a random index to swap ("shuffle") the cards
	for(int i = 0; i < CARDS; i++)
	{
		int j = rand()%CARDS; 						// Random index to shuffle
		Card * otherPtr = &shuffledDeck[j]; 		// Other Card pointer used for swapping
		
		(*temp).cardCopy(otherPtr, "shuffled");		// Perform the swap
		(*otherPtr).cardCopy(&shuffledDeck[i], "shuffled");
		shuffledDeck[i].cardCopy(temp, "shuffled");
	}

	// Clean up
	delete temp;
	return true;
}


void printDeck(Card * deck, int deckLength)
{
	for(int i = 0; i < deckLength; i++)
		deck[i].printCard();
}

void printPlayers(Player * thePlayers, int numPlayers)
{
	for(int i = 0; i < numPlayers; i++)
		thePlayers[i].printPlayer();
}