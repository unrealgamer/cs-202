#include <iostream>
#include "CardPlayer.h"
#include "CStrings.h"

using namespace std;

 
Card::Card()
{
	mRank = 0;
	mSuit = 0;
	mLocation = 0;

	setRank("rank");
	setSuit("suit");
	setLocation("location");

	mValue = 0;
}

Card::Card(const char * rank, const char  * suit, const char * loc, const int value)
{
	mRank = 0;
	mSuit = 0;
	mLocation = 0;

	setRank(rank);
	setSuit(suit);
	setLocation(loc);

	mValue = value;
}

Card::Card(const Card &copy)
{
	mRank = 0;
	mSuit = 0;
	mLocation = 0;

	(*this) = copy;
}


Card::~Card()
{
	delete [] mRank; mRank = 0;
	delete [] mSuit; mSuit = 0;
	delete [] mLocation; mLocation = 0;
}

const char * Card::getRank() const
{
	return mRank;
}

const char * Card::getSuit() const 
{
	return mSuit;
}

const char * Card::getLocation() const
{
	return mLocation;
}

int Card::getValue() const
{
	return mValue;
}

void Card::setRank(const char * rank)
{
	int newLength = strlen(rank);
	// If the pointer is null
	if(mRank == 0)
	{
		// Is the new length less than standard 20
		if(newLength < 20) 
			mRank = new char[20];
		else	// If not resize the array
			mRank = new char[newLength+1];
		strcopy(mRank, rank); // Copy the contents
	}
	else if(newLength > 20)  // If there is a value and the new value is greater
	{
		delete [] mRank; // Delete the old rank
		mRank = createNewString(rank); // Create a new dynmaic string
	}
	else
	{
		strcopy(mRank, rank); // If there is a value and is less than default 20
	}
}

void Card::setSuit(const char * suit)
{
	int newLength = strlen(suit);
	if(mSuit == 0)
	{
		if(newLength < 20)
			mSuit = new char[20];
		else
			mSuit = new char[newLength+1];
		strcopy(mSuit, suit);
	}
	else if(newLength > 20)
	{
		delete [] mSuit;
		mSuit = createNewString(suit);
	}
	else
	{
		strcopy(mSuit, suit);
	}
}

void Card::setLocation(const char * loc)
{
	int newLength = strlen(loc);
	if(mLocation == 0)
	{
		if(newLength < 20)
			mLocation = new char[20];
		else
			mLocation = new char[newLength+1];
		strcopy(mLocation, loc);
	}
	else if(newLength > 20)
	{
		delete [] mLocation;
		mLocation = createNewString(loc);
	}
	else
	{
		strcopy(mLocation, loc);
	}	
}

void Card::setValue(int value)
{
	mValue = value;
}

void Card::printCard() const
{
	cout << getRank() << " " << getSuit() << " (" << getValue() << ") ";
	cout << "Location: " << getLocation() << endl;
}

void Card::cardCopy(Card * source, const char * location)
{
	(*this) = *(source);
	setLocation(location);
	(*source).setLocation(location);
}

Card & Card::operator=(const Card &rhs)
{
	if(&rhs == this)
		return *this;

	this->setSuit(rhs.getSuit());
	this->setRank(rhs.getRank());
	this->setLocation(rhs.getLocation());
	this->setValue(rhs.getValue());

	return *this;
}

Player::Player()
{
	mName = 0;
	setName("first last");

	mHandSize = 3;

	mDeck = new Card[mHandSize];

	mTotal = 0;
}

Player::Player(const int handSize)
{
	mName = 0;
	setName("first last");

	mHandSize = handSize;

	mDeck = new Card[mHandSize];

	mTotal = 0;
}

Player::Player(const int handSize, const char * name)
{
	mName = 0;
	setName(name);

	mHandSize = handSize;

	mDeck = new Card[mHandSize];

	mTotal = 0;
}

Player::~Player()
{
	delete [] mName; mName = 0;
	delete [] mDeck; mDeck = 0;
}


const char * Player::getName() const
{
	return mName;
}

void Player::setName(const char * name)
{
	int newLength = strlen(name);
	if(mName == 0)
	{
		if(newLength < 20)
			mName = new char[20];
		else
			mName = new char[newLength+1];
		strcopy(mName, name);
	}
	else if(newLength > 20)
	{
		delete [] mName;
		mName = createNewString(name);
	}
	else
	{
		strcopy(mName, name);
	}
}

const Card * Player::getDeck() const
{
	return mDeck;
}

int Player::getDeckSize() const
{
	return mHandSize;
}

const Card * Player::getCardAt(int i) const
{ 
	return &mDeck[i];
}

bool Player::copyCardAt(int i, const Card &card)
{
	if( i < 0 && i >= mHandSize)
		return false;

	mDeck[i] = card;

	setTotal(getTotal() + card.getValue());

	mDeck[i].setLocation(mName);

	return true;
}

int Player::getTotal() const
{
	return mTotal;
}

void Player::setTotal(int total)
{
	mTotal = total;
}

void Player::printPlayer() const
{
	cout << mName << " Total: " << mTotal << endl;

	cout << "\tHand: " << endl;

	for(int i = 0; i < mHandSize; i++)
	{
		cout << "\t";
		mDeck[i].printCard();
	}

}