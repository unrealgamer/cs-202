#include <iostream>

#include "CardPlayer.h"
#include "CStrings.h"

using namespace std;



int main()
{

	Card * c = new Card("Two", "Heart", "location", 2);

	Card c2 = *c; 

	c->printCard();

	c2.setSuit("Diamond");

	c2.printCard();

	c->printCard();

	return 0;
}