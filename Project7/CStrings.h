/**
	Concatenates two C style strings together which is stored in the dest argument
	@param dest The destination for the concatenation and the first portion of the concatenated string
	@param source The source for the concatentation that will be appended to the destination string
*/
void strcat(char * dest, const char * source);

/**
	Copys the conents from the source C string to the destination C string
	@param dest The destination for the source to be copied too
	@param source The source to be copied	
*/
void strcopy(char * dest, const char * source);

/**
	Measures and returns the length of the provided C style string (does not include the null character)
	@param str The string to measure
	@return The length of the string (does not include null character)
*/
int strlen(const char * str);

/**
	Compares two C style strings (is Case Sensitive)
	@param one First string to compare
	@param two Second string to compare
	@return True if equivalent, False otherwise
*/
bool strcomp(const char * one, const char * two);

/**
	Takes the provided string and creates a new dynamically allocated string
	@param str The string to copy into the new string
*/
char * createNewString(const char * str);