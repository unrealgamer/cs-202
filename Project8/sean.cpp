#include<string>
#include<iostream>
using namespace std;

#ifndef REPORT_H
#define REPORT_H
#include <string>

class Report
{
public:
	Report(int, int, int, int, int, std::string);
	void display();
private:
	int _month;
	int _day;
	int _year;
	int _hour;
	int _minute;
	std::string _description;
};

#endif

int main()
{
  Report r(7,4,1776,10,15, "Founding fathers take a coffee break.");
  r.display();
}
/*==========================================================================*/

Report::Report(int month, int day, int year, int hour, int minute, string description)
	: _month(month),  _day(day),  _year(year),  _hour(hour),  _minute(minute), _description(description)
{

}

void Report::display()
{
	cout<<"Hello"<<endl;
}
/*==========================================================================*/

/*==========================================================================*/