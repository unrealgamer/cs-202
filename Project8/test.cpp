#include <iostream>

#include "CardPlayer.h"
#include "CStrings.h"

#include <fstream>

using namespace std;

int main()
{

	Card * c1 = new Card("Two", "Heart", " ", 2);
	Card * c2 = new Card("Two", "Spade", " ", 2);

	cout << "c1 > c2 = " << (*c1 > *c2) << endl;

	cout << c1->getSuit() << endl;

	return 0;
}