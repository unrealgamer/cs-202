#include <iostream>
#include <istream>
#include <fstream>
#include "CardPlayer.h"
#include "CStrings.h"

using namespace std;

Card::Card(const char * rank, const char  * suit, const char * loc, const int value)
{
	mRank = 0;
	mSuit = 0;
	mLocation = 0;

	setRank(rank);
	setSuit(suit);
	setLocation(loc);

	mValue = value;
}

Card::Card(const Card &copy)
{
	mRank = 0;
	mSuit = 0;
	mLocation = 0;

	(*this) = copy;
}


Card::~Card()
{
	delete [] mRank; mRank = 0;
	delete [] mSuit; mSuit = 0;
	delete [] mLocation; mLocation = 0;
}

const char * Card::getRank() const
{
	return mRank;
}

const char * Card::getSuit() const 
{
	return mSuit;
}

const char * Card::getLocation() const
{
	return mLocation;
}

int Card::getValue() const
{
	return mValue;
}

void Card::setRank(const char * rank)
{
	int newLength = strlen(rank);
	// If the pointer is null
	if(mRank == 0)
	{
		// Is the new length less than standard 20
		if(newLength < 20) 
			mRank = new char[20];
		else	// If not resize the array
			mRank = new char[newLength+1];
		strcopy(mRank, rank); // Copy the contents
	}
	else if(newLength > 20)  // If there is a value and the new value is greater
	{
		delete [] mRank; // Delete the old rank
		mRank = createNewString(rank); // Create a new dynmaic string
	}
	else
	{
		strcopy(mRank, rank); // If there is a value and is less than default 20
	}
}

void Card::setSuit(const char * suit)
{
	int newLength = strlen(suit);
	if(mSuit == 0)
	{
		if(newLength < 20)
			mSuit = new char[20];
		else
			mSuit = new char[newLength+1];
		strcopy(mSuit, suit);
	}
	else if(newLength > 20)
	{
		delete [] mSuit;
		mSuit = createNewString(suit);
	}
	else
	{
		strcopy(mSuit, suit);
	}
}

void Card::setLocation(const char * loc)
{
	int newLength = strlen(loc);
	if(mLocation == 0)
	{
		if(newLength < 20)
			mLocation = new char[20];
		else
			mLocation = new char[newLength+1];
		strcopy(mLocation, loc);
	}
	else if(newLength > 20)
	{
		delete [] mLocation;
		mLocation = createNewString(loc);
	}
	else
	{
		strcopy(mLocation, loc);
	}	
}

void Card::setValue(int value)
{
	mValue = value;
}

void Card::cardCopy(Card * source, const char * location)
{
	(*this) = *(source);
	setLocation(location);
	(*source).setLocation(location);
}

Card & Card::operator=(const Card &rhs)
{
	if(&rhs == this)
		return *this;

	this->setSuit(rhs.getSuit());
	this->setRank(rhs.getRank());
	this->setLocation(rhs.getLocation());
	this->setValue(rhs.getValue());

	return *this;
}

bool Card::operator==(const Card &rhs) const
{
	return strcomp(getSuit(), rhs.getSuit()) && 
			strcomp(getRank(), rhs.getRank()) && 
			strcomp(getLocation(), rhs.getLocation()) &&
			getValue() == rhs.getValue();
}

bool Card::operator!=(const Card &rhs) const
{
	return !(operator==(rhs));
}

bool operator<(const Card &lhs, const Card &rhs)
{	
 	if(lhs.getValue() == rhs.getValue()) // If Cards have the same rank
		return ((int)(lhs.getSuit())[0]) < ((int)(rhs.getSuit())[0]); //Pick the higher suit (alphabetical)
	else
		return lhs.getValue() < rhs.getValue();
}

bool Card::operator>(const Card &rhs) const
{
	return !(operator<(*this, rhs));
}

bool Card::operator>=(const Card &rhs) const
{
	return this->operator==(rhs) || this->operator>(rhs);
}

bool Card::operator<=(const Card &rhs) const
{
	return this->operator==(rhs) || operator<(*this, rhs);
}

ostream & operator<<(ostream &os, const Card &card)
{
	os << card.getRank() << " " << card.getSuit() << " (" << card.getValue() << ") ";
	os << "Location: " << card.getLocation();
	return os;
}

istream & operator>>(istream &is, Card &card)
{
	char * temp = new char[50];
	is >> temp;
	card.setSuit(temp);
	is >> temp;
	card.setRank(temp);

	int tempV = 0;
	is >> tempV;
	card.setValue(tempV);

	card.setLocation("unshuffled");

	delete [] temp;

	return is;
}

Player::Player(const int handSize, const char * name)
{
	mName = 0;
	setName(name);

	mHandSize = handSize;

	mDeck = new Card[mHandSize];

	mTotal = 0;
}

Player::~Player()
{
	delete [] mName; mName = 0;
	delete [] mDeck; mDeck = 0;
}


const char * Player::getName() const
{
	return mName;
}

void Player::setName(const char * name)
{
	int newLength = strlen(name);
	if(mName == 0)
	{
		if(newLength < 20)
			mName = new char[20];
		else
			mName = new char[newLength+1];
		strcopy(mName, name);
	}
	else if(newLength > 20)
	{
		delete [] mName;
		mName = createNewString(name);
	}
	else
	{
		strcopy(mName, name);
	}
}

const Card * Player::getDeck() const
{
	return mDeck;
}

int Player::getDeckSize() const
{
	return mHandSize;
}

const Card * Player::getCardAt(int i) const
{ 
	return &mDeck[i];
}

bool Player::copyCardAt(int i, const Card &card)
{
	if( i < 0 && i >= mHandSize)
		return false;

	mDeck[i] = card;

	setTotal(getTotal() + card.getValue());

	mDeck[i].setLocation(mName);

	return true;
}

int Player::getTotal() const
{
	return mTotal;
}

void Player::setTotal(int total)
{
	mTotal = total;
}


bool Player::operator==(const Player &rhs) const
{
	return strcomp(getName(), rhs.getName());
}

bool Player::operator!=(const Player &rhs) const
{
	return !(operator==(rhs));
}

ostream& operator<<(ostream &os, const Player &player)
{
	os << player.getName() << " Total: " << player.getTotal() << endl;

	os << "\tHand: " << endl;

	for(int i = 0; i < player.getDeckSize(); i++)
	{
		os << "\t" << player.getDeck()[i] << endl;
	}

	return os;
}


istream& operator>>(istream &is, Player &player)
{

	char * temp = new char[50];
	char * tempL = new char[50];

	is >> temp;
	is >> tempL;
	strcat(temp, " ");
	strcat(temp, tempL);
	player.setName(temp);

	delete [] temp;
	delete [] tempL;

	return is;
}