#include <ostream>
#include <fstream>

using namespace std;

class Card {
public:
	/**
		Constructor the creates a card with the provided parameters
		@param char* 	Rank
		@param char* 	Suit
		@param char* 	Location
		@param int		cValue
	*/
	Card(const char * = "rank", const char * = "suit", const char * = "location", const int = 0);

	/**
		Copy Constructor
		@param &Card 	The card to copydddddddddddddddddddddddd
	*/
	Card(const Card &);

	/**
		Deconstructor
	*/
	~Card();

public:
	/**
		Getters and Setters
		+++
		The getters return const pointers so that the member cannot
		be changed without the owner knowing about it.
		+++
	*/
	const char * 	getRank() const;
	const char * 	getSuit() const;
	const char * 	getLocation() const;
	int 			getValue() const;
	void			setRank(const char *);
	void			setSuit(const char *);
	void			setLocation(const char *);
	void			setValue(int);

	/**
		Copys the provided card's values into the caller of the method, the location will also be set
		@param Card* 	The source card to copy values from
		@param char*	The new location for this card
	*/
	void			cardCopy(Card * source, const char * loc);

	/**
		Assignment Overload that will copy all values from the source and place them in the right hand side (rhs) operand
		@param Card		The source card to copy values from
		@return Card 	The current card that will have equivalent values as the rhs.
	*/
	Card &			operator=(const Card &rhs);

	bool			operator==(const Card &rhs) const;
	bool			operator!=(const Card &rhs) const;

	friend bool		operator<(const Card &lhs, const Card &rhs);
	bool			operator>(const Card &rhs) const;
	bool			operator>=(const Card &rhs) const;
	bool			operator<=(const Card &rhs) const;

	friend ostream&	operator<<(ostream &os, const Card &card);
	friend istream& operator>>(istream &is, Card &card);


private:
	/**
		Private members of the Card
	*/
	char *		mRank;
	char *		mSuit;
	char * 		mLocation;
	int			mValue;
};

class Player {
public:
	/**
		Default Constructor with defaults
	*/
	Player(const int handSize = 3, const char * name = "first last");
	/**
		Player Deconstructor
	*/
	~Player();

public:
	/**
		Getters and Setters
		+++
		The getters return const pointers so that the member cannot
		be changed without the owner knowing about it.
		+++
	*/
	const char *		getName() const;
	void 				setName(const char *);
	const Card *		getDeck() const;
	int					getDeckSize() const;
	const Card *		getCardAt(int) const;
	int					getTotal() const;
	void				setTotal(int);

	/**
		Copies the provided card's information to the position in the player's deck specified
		@param int 	The position in the player's hand
		@param Card The card to copy into the player's hand
	*/
	bool				copyCardAt(int, const Card &);

	bool				operator==(const Player &rhs) const;
	bool				operator!=(const Player &rhs) const;

	friend ostream&		operator<<(ostream &os, const Player &player);
	friend istream& 	operator>>(istream &is, Player &player);


private:
	char *		mName;
	Card *		mDeck;
	int 		mHandSize;
	int			mTotal;
};