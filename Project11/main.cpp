#include "listN.h"
#include "listA.h"
#include <iostream>
#include <fstream>

using namespace std;

/**
*	Takes a list and swaps the value at the current cursor postion with the previous
*	@param ListN The list to use with swaps
*	@return bool True on succes
*/
bool swapPrior(ListN&);
/**
*	Takes the provided list and sorts it by ascending value
*	@param The list to sort
*	@return bool True on success
*/	
bool sort(ListN&);
/*
*	Copys the value of the previous two elements from the list into the parameters
*	@param int& First output param
*	@param int& Second output param
*	@param List& List to get the values from
* 	@return bool True on success
*/
bool getPreviousTwoVals(int&, int&, ListN&);

/**
*	Performs a binary search on the provided Array Based List
*	@param int 		The item to search for
*	@param ListA& 	The list to search through
*	@param int 		The starting index of the search
*	@return int 	The index of the found element (-1 if not found)
*/
int search(int, ListA&, int start, int end);

/**
*	Gets the value in the provided list at the provided index
*	@param int The index to use
*	@param ListA& The list to use
*	@return int The value that was found
*/
int getValueAt(int, ListA&);

// Max numbers allowed in the array and which are in the data file
int MAX_NUMBERS = 20;

int main() {
	
	/**** Read the data in *****/
	ifstream ifs ("nums.txt");

	if(!ifs.is_open())
	{
		cout << "Error Openning File!" << endl;
		return 1;
	}

	int temp;

	ListN theList(MAX_NUMBERS);

	while((ifs >> temp))
	{
		theList.insertAfter(temp);
	}

	/******* Print the unsorted and sorted lists *******/

	cout << "UnSorted List: " << endl;
	cout << theList << endl;

	// Sort the list
	sort(theList);

	cout << "Sorted List: " << endl;
	cout << theList << endl;

	cout << "===Copy List into Array Based List===" << endl;

	ListA theListA(MAX_NUMBERS);
	theList.gotoBeginning();

	for(int i = 0; i < MAX_NUMBERS; i++)
	{
		theList.getCursor(temp);
		theListA.insertAfter(temp);
		theList.gotoNext();
	}

	theListA.gotoBeginning();

	cout << "Searching for 70 found at index: ";

	int index = search(70, theListA, 0, MAX_NUMBERS);

	cout << index << endl;

	return 0;
}

int search(int s, ListA& list, int start, int end) {
	
	int mid = (end + start) / 2; 			// Get the middle index

	int midVal = getValueAt(mid, list);		// Get the middle value

	if(s == midVal)							// If the mid val is the search term
		return mid;							// Return
	else if(s > midVal)
		return search(s, list, mid, end);  	// Search up if the value is greater
	else if(s < midVal)
		return search(s, list, start, mid);	// Search down if the value is less

	return -1; // Return -1 if not found
}

int getValueAt(int index, ListA& list)
{
	list.gotoBeginning();
	for(int i = 0; i < index; i++)
		list.gotoNext(); // Iterate up to the requested index
	int a;
	list.getCursor(a);
	return a;
}

bool sort(ListN& list)
{
	list.gotoBeginning();

	// Bool to keep track of swaps that occured
	bool swap = true;

	// Continue looping through the array until no more swaps occur
	while(swap)
	{
		swap = false; // Set swap to false for each loop
		while(list.gotoNext())
		{
			int a, b;
			getPreviousTwoVals(a,b,list);

			if(a < b) // If the two values are out of order
				if(swapPrior(list)) // Swap them and set swap to true
					swap = true;
		}
		list.gotoBeginning(); // Got back to the start of the list for the next iteration
	}
	return true;

}

bool getPreviousTwoVals(int& a, int &b, ListN& list)
{
	list.getCursor(a); 		// Set first param
	if(!list.gotoPrior())	// Attempt to go back
		return false;

	list.getCursor(b);		// Get the previous value
	list.gotoNext();		// Got back to original position

	return true;
}

bool swapPrior(ListN& list)
{
	int a,b;

	list.getCursor(a);		// Set first temp
	if(!list.gotoPrior())	// Attempt to go back
		return false;

	list.getCursor(b);		// Get second temp
	list.replace(a);		// Replace second value with first temp
	list.gotoNext();		// Goto orignal
	list.replace(b);		// Replace first value with second temp

	return true;
}