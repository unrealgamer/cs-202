#include "listN.h"

// Node Constructor
Node::Node(int c, Node * n)
{
	this->data = c;
	this->next = n;
}

// List constructor
List::List(int size)
{
	this->cursor = NULL;
	this->head = NULL;
}

// List Copy Constructor
List::List(const List& list)
{
	(*this) = list;
}

// List desconstructor
List::~List()
{
	clear();
}

// Sets the cursor pointer to the head pointer
bool List::gotoBeginning()
{
	this->cursor = this->head;
	return true;
}

// Sets the cursor pointer to the last element in the list
bool List::gotoEnd()
{
	if(empty())
		return true;
	while(gotoNext());
	return true;
}

// Moves the cursor pointer to the next element in the list
bool List::gotoNext() 
{
	if(empty())
		return false; 

	if(this->cursor->next != NULL)
		this->cursor = this->cursor->next;
	else
		return false;
	return true;
}

// Moves the cursor pointer to the previous element in the list
bool List::gotoPrior() 
{

	if(empty())
		return false;

	Node * temp = this->head;

	if(this->cursor == this->head)
		return true;

	while(temp->next != this->cursor)
		temp = temp->next;

	this->cursor = temp;

	return true;
}

// Inserts the provided intacter into the list after the cursor
// and moves the cursor to the new element
bool List::insertAfter(int c) 
{

	if(empty())
	{
		this->head = new Node(c, NULL);
		this->cursor = this->head;
		return true;
	}

	if(this->cursor->next == NULL)
	{
		this->cursor->next = new Node(c, NULL);
		this->cursor = this->cursor->next;
		return true;
	}

	this->cursor->next = new Node(c, this->cursor->next);

	this->cursor = this->cursor->next;

	return true;
}

// Inserts the provided intacter into the list before the cursor
// and moves the cursor to the new element
bool List::insertBefore(int c) 
{
	if(empty())
	{
		this->head = new Node(c, NULL);
		this->cursor = this->head;
		return true;
	}

	if(this->cursor == this->head)
	{
		this->head = new Node(c, this->cursor);
		this->cursor = this->head;
		return true;
	}

	gotoPrior();

	insertAfter(c);

	return true;
}

// Removes the element at the cursor and fixes the break in the chain of nodes
bool List::remove(int& o) 
{

	getCursor(o);

	if(this->cursor == this->head && this->cursor->next == NULL)
	{
		delete this->cursor;
		this->head = NULL;
		this->cursor = NULL;
		return true;
	}
	else if(this->cursor == this->head)
	{
		this->head = this->cursor->next;
		delete this->cursor;
		this->cursor = this->head;
		return true;
	}

	Node * nDelete = this->cursor;

	gotoPrior();

	this->cursor->next = nDelete->next;

	delete nDelete; nDelete = NULL;

	if(this->cursor->next == NULL)
		gotoBeginning();
	else
		this->cursor = this->cursor->next;


	return true;
}

// Replaces the data at the cursor with the provided cursor
bool List::replace(int c) 
{
	this->cursor->data = c;
	return true;
}

// Gets the data at the intacter and sets it to the provided argument
bool List::getCursor(int& o) const
{
	o = this->cursor->data;
	return true;
}

// Checks if the list is empty
bool List::empty() const
{
	return this->head == NULL;
}

// The list has no limit so always return false
bool List::full() const
{
	return false;
}	

// Empties the list by deleting all of the nodes and clearing the pointers
bool List::clear()
{
	if(empty())
		return true;

	gotoEnd();
	gotoPrior();

	while(this->cursor->next != NULL)
	{
		delete this->cursor->next;
		this->cursor->next = NULL;
		gotoPrior();
	}

	delete this->cursor;
	this->cursor = NULL;
	this->head = NULL;

	return true;	
}

// Assignment operator that deep copies the provided list
List& List::operator=(const List& list)
{	

	Node * src = list.head;

	Node * tempCursor = NULL;

	this->head = new Node(list.head->data, NULL);
	this->cursor = this->head;

	while(src->next != NULL)
	{
		if(src == list.cursor)
			tempCursor = this->cursor;

		this->cursor->next = new Node(src->next->data, NULL);
		this->cursor = this->cursor->next;
		src = src->next;
	}

	this->cursor = tempCursor;

	return *this;
}

// Insertion operator that prints the list to the output stream
ostream& operator<<(ostream& out, const List& list)
{
	if(list.empty())
	{
		out << "Empty!";
		return out;
	}
	Node * temp = list.head;	
	while(temp->next != NULL)
	{
		out << temp->data << ", ";
		temp = temp->next;
	}

	out << temp->data;
	return out;
}

// Comparison operator the compares the lists
bool List::operator==(const List& list) const
{
	Node * org = list.head;
	Node * oth = this->head;

	while(org->next != NULL && oth->next != NULL)
	{

		if((list.cursor == org && this->cursor != oth) || (this->cursor == oth && list.cursor != org))
			return false;

		if(org->data != oth->data)
			return false;
		org = org->next;
		oth = oth->next;
	}

	if(oth->data != org->data)
		return false;

	return true;
}