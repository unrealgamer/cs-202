#include "listA.h"

// ListA constructor
ListA::ListA(int size)
{
	this->size = size;
	this->actual = 0;
	this->cursor = 0;
	this->data = new int[this->size];
}

// ListA Copy Constructor
ListA::ListA(const ListA& list)
{
	(*this) = list;
}

// ListA desconstructor
ListA::~ListA()
{
	delete [] this->data;
}

// Sets the cursor index to 0
bool ListA::gotoBeginning()
{
	this->cursor = 0;
	return true;
}

// Sets the cursor index to the last element in the list
bool ListA::gotoEnd()
{
	this->cursor = this->actual-1;
	return true;
}

// Moves the cursor to the next element in the list
bool ListA::gotoNext() 
{
	if(this->cursor == this->actual-1)
		return false;
	else
		this->cursor++;
	return true;
}

// Moves the cursor to the previous element in the list
bool ListA::gotoPrior() 
{
	if(this->cursor <= 0)
		return false;
	else
		this->cursor--;
	return true;
}

// Inserts the provided intacter into the list after the cursor
// and moves the cursor to the new element
bool ListA::insertAfter(int c) 
{
	if(full())
		return false;

	for(int i = this->actual-1; i > this->cursor; i--)
	{
		this->data[i+1] = this->data[i];
	}

	if(!empty())
		this->cursor++;

	if(!replace(c))
		return false;

	this->actual++;

	return true;
}

// Inserts the provided intacter into the list before the cursor
// and moves the cursor to the new element
bool ListA::insertBefore(int c) 
{
	if(full())
		return false;

	// if(!gotoPrior() && this->actual != 0)
	// 	return false;

	for(int i = this->actual-1; i >= this->cursor; i--)
	{
		this->data[i+1] = this->data[i];
	}

	if(!replace(c))
		return false;

	this->actual++;

	return true;
}

//// Removes the element at the cursor and shuffles the list
bool ListA::remove(int& o) 
{
	if(!getCursor(o))
		return false;

	for(int i = this->cursor; i < this->actual-1; i++)
	{
		this->data[i] = this->data[i+1];
	}

	if(this->cursor == this->actual-1)
		gotoBeginning();

	//this->data[this->cursor] = '\0';
	this->actual--;
	return true;
}

// Replaces the data at the cursor with the provided cursor
bool ListA::replace(int c) 
{
	this->data[this->cursor] = c;
	return true;
}

// Gets the data at the intacter and sets it to the provided argument
bool ListA::getCursor(int& o) const
{
	if(empty())
		return false;
	if(this->cursor < 0 || this->cursor >= this->actual)
		return false;
	o = this->data[this->cursor];
	return true;
}

// Checks if the list is empty
bool ListA::empty() const
{
	return this->actual == 0;
}

// The list has no limit so always return false
bool ListA::full() const
{
	return this->actual == size;
}	

// Empties the list by deleting all of the data
bool ListA::clear()
{
	this->cursor = 0; 
	this->actual = 0;
	delete [] this->data;
	this->data = new int[this->size];
	return true;	
}

// Assignment operator that deep copies the provided list
ListA& ListA::operator=(const ListA& list)
{

	this->cursor 	= list.cursor;
	this->size 		= list.size;
	this->actual 	= list.actual;

	delete [] this->data;

	this->data = new int[list.size];

	for(int i = 0; i < list.actual; i++)
	{
		this->data[i] = list.data[i];
	}

	return *this;
}

// Insertion operator that prints the list to the output stream
ostream& operator<<(ostream& out, const ListA& list)
{
	for(int i = 0; i <= list.actual-2; i++)
	{
		out << list.data[i] << ", ";
	}

	out << list.data[list.actual-1];

	//int c;

	//list.getCursor(c);

	//out << "\t\t Cursor: " << list.cursor << " (" << c << ")";

	return out;
}

// Comparison operator the compares the lists
bool ListA::operator==(const ListA& list) const
{

	if(this->cursor != list.cursor || this->size != list.size || this->actual != list.actual)
		return false;

	for(int i = 0; i < this->actual; i++)
		if(this->data[i] != list.data[i])
			return false;
	
	return true;
}