
class Card {
public:
	/**
		Default Constructor
	*/
	Card();
	/**
		Constructor the creates a card with the provided parameters
		@param char* 	Rank
		@param char* 	Suit
		@param char* 	Location
		@param int		cValue
	*/
	Card(const char *, const char *, const char *, const int);
	/**
		Deconstructor
	*/
	~Card();

public:
	/**
		Getters and Setters
	*/
	const char * 	getRank() const;
	const char * 	getSuit() const;
	const char * 	getLocation() const;
	int				getValue() const;
	void			setRank(const char *);
	void			setSuit(const char *);
	void			setLocation(const char *);
	void			setValue(int);

	/**
		Prints the cards on a new line
	*/
	void			printCard() const;

	/**
		Copys the provided card's values into the caller of the method, the location will also be set
		@param Card* 	The source card to copy values from
		@param char*	The new location for this card
	*/
	void			cardCopy(Card * source, const char * loc);

	/**
		Assignment Overload that will copy all values from the source and place them in the right hand side (rhs) operand
		@param Card		The source card to copy values from
		@return Card 	The current card that will have equivalent values as the rhs.
	*/
	Card &			operator=(const Card &rhs);

private:
	/**
		Private members of the Card
	*/
	char *		mRank;
	char *		mSuit;
	char * 		mLocation;
	int			mValue;
};

class Player {
public:
	/**
		Default Constructor
	*/
	Player();
	/**
		Constructor that takes the player's hand size
		@param deckSize The size of the player's hand	
	*/
	Player(const int deckSize);
	/**
		Constructor that takes the player's hand size and name
		@param deckSize The size of the player's hand
		@param name 	The name of the player
	*/
	Player(const int deckSize, const char * name);
	/**
		Player Deconstructor
	*/
	~Player();

public:
	/**
		Getters and Setters
	*/
	const char * 		getName();
	void 				setName(const char *);
	Card *				getDeck();
	int					getDeckSize();
	Card *				getCardAt(int);
	int					getTotal();
	void				setTotal(int);

	/**
		Copies the provided card's information to the position in the player's deck specified
		@param int 	The position in the player's hand
		@param Card The card to copy into the player's hand
	*/
	bool				copyCardAt(int, const Card &);
	/**
		Prints the player's variables
	*/
	void				printPlayer() const;

private:
	char *		mName;
	Card *		mDeck;
	int 		mDeckSize;
	int			mTotal;
};