#include <iostream>

using namespace std;

struct Card
{
	char * suit;
	char * rank;
	int cvalue;
	char * location;
};

int strlen(char const * str)
{
	int length = 0;
	while(*str++)
		length++;
	return length;
}

void strcopy(char * dest, const char * source)
{
	// Start the source pointer at the beginning
	while(*source)
	{
		*dest = *source; // Copy the source character to the desination
		dest++; // increment pointer
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}

char * createDynamicString(char const * source)
{
	char * ret = new char[strlen(source) + 1];
	strcopy(ret, source);
	return ret;
}


void cardCopy(Card * dest, Card * source, const char * newLocation)
{

	delete [] (*dest).suit; (*dest).suit = NULL;
	delete [] (*dest).rank; (*dest).rank = NULL;
	delete [] (*dest).location; (*dest).location = NULL;
	delete [] (*source).location; (*source).location = NULL;

	(*dest).suit = createDynamicString((*source).suit);
	(*dest).rank = createDynamicString((*source).rank);
	(*dest).location = createDynamicString(newLocation);
	(*source).location = createDynamicString(newLocation);

	(*dest).cvalue = (*source).cvalue;

	// // Copy each source to the respective destination
	// strcopy(dest.suit, source.suit);
	// strcopy(dest.rank, source.rank);
	// strcopy(dest.location, newLocation);
	// strcopy(source.location, newLocation);
	// dest.cvalue = source.cvalue;
}


void deleteDeck(Card * deck, int deckSize)
{
	Card * card = deck;
	for(int i = 0; i < deckSize; i++, card++)
	{
		delete [] (*card).suit; (*card).suit = NULL;
		delete [] (*card).rank;	(*card).rank = NULL;
		delete [] (*card).location; (*card).location = NULL;
	}
	delete [] deck; deck = NULL;
}

void setDeckDefaults(Card * deck, int deckLength)
{
	// Card Pointer for iterating through the deck
	Card * cptr = deck;

	// Loop through and increment pointer and counter
	for(int i = 0; i < deckLength; cptr++, i++)
	{
		// Set default values
		(*cptr).suit = createDynamicString("suit");
		(*cptr).rank = createDynamicString("rank");
		(*cptr).location = createDynamicString("location");
		(*cptr).cvalue = i;
	}
}


void shuffle(Card * deck, int j)
{

}

void doSomething(Card * deck)
{
	Card * cptr = deck;

	for(int i = 0; i < 52; i++, cptr++)
	{
		delete [] (*cptr).suit; (*cptr).suit = NULL;
		delete [] (*cptr).rank;	(*cptr).rank = NULL;
		delete [] (*cptr).location; (*cptr).location = NULL;

		(*cptr).suit = createDynamicString("Suit!");
		(*cptr).rank = createDynamicString("Rank!");
		(*cptr).location = createDynamicString("Location!");
	}

}

void printCard(Card &card)
{
	cout << card.suit << " " << card.rank << " Value: " << card.cvalue << " Loc: " << card.location << endl;
}

int main(int argc, char * argv[])
{

	Card * deckOne = new Card[52];
	Card * deckTwo = new Card[52];

	setDeckDefaults(deckOne, 52);
	setDeckDefaults(deckTwo, 52);

	doSomething(deckOne);

	Card * cptr = deckOne;
	cout << "Deck One: " << endl;
	for(int i = 0; i < 52; i++, cptr++)
	{
		cout << '\t'; printCard(*cptr);// << endl;
	}

	cptr = deckTwo;

	cout << "Deck Two: " << endl;
	for(int i = 0; i < 52; i++, cptr++)
	{
		cout << '\t'; printCard(*cptr);
	}

	deleteDeck(deckOne, 52);
	deleteDeck(deckTwo, 52);

	return 0;
}
