/***************************************************************/
/* PROGRAM: Project 1 Card Shuffler                                                                  */
/*                                                                                       */
/* AUTHOR:  Shane Melton                                                                    */
/*                                                                                       */
/* DATE:    September 11, 2014                                                                        */
/*                                                                                      */
/* REVISIONS: 1                                                                */
/*                                                                                      */
/* PURPOSE: A small program that will read a deck of 52 cards and shuffle and deal them out. */
/*                                                                                      */
/**************************************************************/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

// Constants for the number of cards in a deck, number of players, and the number of cards in a hand

const int CARDS = 52;
const int PLAYERS = 4;
const int CARDS_PER_HAND = 3;

// A struct that represents a custom Card datatype
struct Card
{
	char 	suit[20];
	char 	rank[20];
	int 	cvalue;
	char 	location[30];
};

// A struct that represents a custom Player datatype
struct Player
{
	char 	name[500];
	int 	total;
	Card 	hand[CARDS_PER_HAND];
};

// Function Prototypes

/**
	Prints the menu to the console. (Does not wait for any user input)
*/
void printMenu();

/**
	Loads a deck of cards from a file and stores them into the single dimensiaonal array provided
	@param Card[] The array to be filled with cards
*/
bool loadUnshuffledDeck(Card[]);

/**
	Loads a list of players from a file and stores them into the single dimensiaonal array provided
	@param Player[] The array to be filled with players
*/
bool loadPlayers(Player[]);

/**
	Clears out (resets) all of the players' deck and their total hand score
	@param Player[] The array of players
*/
void setPlayerDeckDefaults(Player[]);

/**
	Resets the deck (array) by filling it with a default card value
	@param Card[] The deck to reset
	@param deckLength The length of the provided deck
*/
void setDeckDefaults(Card[], int deckLength = CARDS);

/**
	Takes cards from the unshuffled deck (which never changes after load) and randomly orders
	them within the shuffled deck.
	@param shuffledDeck[] The deck to be shuffled
	@param unShuffledDeck[] The static deck used to populate the shuffled deck
*/
bool shuffleDeck(Card shuffledDeck[], Card unShuffledDeck[]);

/**
	Simulates a card dealing using SCAT rules. Each player is dealt three cards in a round robin fashion
	Then one card is placed in the discard pile, will the rest are 'placed' in the stock pile.
	@param shuffledDeck The source deck
	@param players The array of players to deal to
	@param stock The stock deck
	@param discard The discard deck
*/
bool dealCards(Card shuffled[], Player players[], Card stock[], Card discard[]);

/**
	Prints a card and all of its attributes to the console
	@param Card The card to print
*/
void printCard(Card);

/**
	Prints all of the players to screen including their hand and other attributes
	@param Playerp[] The array of players
*/
void printPlayers(Player[]);

/**
	Prints every deck to screen
	@param Self explanatory
*/
void printAllDecks(Card shuffledDeck[], Card unShuffledDeck[], Card stock[], Card discard[]);

/**
	Does a deep copy of the source card into the destination. Also the card has its location set
	to the provided new location C string.
	@param dest The destination card
	@param source The source card
	@param newLocation The new location for the card (typically the destination deck)
*/
void cardCopy(Card &dest, Card &source, const char newLocation[]);

/**
	Basic string copy function (without pointers this time)
*/
void strcopy(char dest[], const char source[]);

/**
	Basic string concatenation function (without pointers this time)
*/
void strcat(char dest[], const char source[]);

// End Function Prototypes

using namespace std;

// Main entry point of the program
int main() {

	// Integer used to store the users selection in the menus
	int userSel = 0;

	// Booleans to check if the deck and players were loaded succesfully
	bool loadedDeck = false;
	bool loadedPlayers = false;

	bool handDealt = false;
	bool deckShuffled = false;

	// Array for deck and players
	Card unShuffledDeck[CARDS];
	Card shuffledDeck[CARDS];
	Card discardDeck[CARDS];
	Card stockPileDeck[CARDS];

	// Sets every card in each deck to the default card
	setDeckDefaults(unShuffledDeck);
	setDeckDefaults(shuffledDeck);
	setDeckDefaults(discardDeck);
	setDeckDefaults(stockPileDeck);

	Player thePlayers[PLAYERS];

	// Sets every players deck and total to their defaults
	setPlayerDeckDefaults(thePlayers);

	while(userSel != 7)
	{
		printMenu(); // Print the menu to the screen

		cin >> userSel; // Wait for the user's input

		if(!cin.good()) // Verify the user's input is an integer
		{
			cout << "Improper Selection! Please try again!" << endl;

			cin.clear(); // Clear the input buffer

			cin.ignore(); // Flush the new line character

			continue;
		}

		cin.ignore(); // Flush the new line character '\n' from the buffer

		switch(userSel)
		{
			case 1: // Load Deck File
			{
				loadedDeck = loadUnshuffledDeck(unShuffledDeck);
				cout << endl;
				break;
			}
			case 2: // Load Player File
			{
				loadedPlayers =	loadPlayers(thePlayers);
				cout << endl;
				break;
			}
			case 3: // Resets each deck including the players. Then shuffles
			{
				if(loadedDeck)
				{
					setDeckDefaults(stockPileDeck);
					setDeckDefaults(discardDeck);
					setPlayerDeckDefaults(thePlayers);
					deckShuffled = shuffleDeck(shuffledDeck, unShuffledDeck);
					cout << "Deck Shuffled Succesfully!" << endl;
				}
				else
					cout << "Please load the deck first. (Option 1)" << endl;
				break; 
			}
			case 4: // Deal to Players
			{
				if(loadedDeck && loadedPlayers && deckShuffled)
				{
					handDealt = dealCards(shuffledDeck, thePlayers, stockPileDeck, discardDeck);
					cout << "Hands Dealt Succesfully" << endl;
				}
				else
					cout << "Please load the deck and players and shuffle the deck first. (Option 1 & 2 & 3)" << endl;
				break;
			}
			case 5: // Print Players and Hands
			{
				if(loadedDeck && loadedPlayers)
					printPlayers(thePlayers);
				else
					cout << "Please load the deck and players first. (Option 1 & 2)" << endl;
				break;
			}
			case 6: // Print all decks to screen
			{
				if(loadedDeck && loadPlayers && handDealt)
					printAllDecks(shuffledDeck, unShuffledDeck, stockPileDeck, discardDeck); 
				else
					cout << "Please make sure everything is loaded and atleast one shuffle has occured." << endl;
				break;
			}
			case 7: // Exit the program
			{
				cout << "Good Bye" << endl;
				return 0;
			}
			default: // Out of range option
			{
				cout << "Please select an option 1-6." << endl;
				break;
			}
		}
		cout << "Press the enter key to continue...";
		cin.ignore();
	}
	return 0;
}


void printMenu()
{
	cout << endl << endl << "Select an option below: " << endl;

	cout << "1. Load Card File" << endl;
	cout << "2. Load Player File" << endl;
	cout << "3. Shuffle / Reset Decks" << endl;
	cout << "4. Deal to Players" << endl;
	cout << "5. Print Players and Hands" << endl;
	cout << "6. Print All Decks" << endl;
	cout << "7. Exit" << endl;

	cout << "Selection: ";
}

bool loadUnshuffledDeck(Card unShuffledDeck[])
{
	//Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the input file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	// Loop through each row and column of the input file
	for(int c = 0; c < CARDS; c++)
	{
		ifs >> unShuffledDeck[c].suit;
		ifs >> unShuffledDeck[c].rank;
		ifs >> unShuffledDeck[c].cvalue;
		strcopy(unShuffledDeck[c].location, "unshuffled");
	}

	//Close the file
	ifs.close();

	cout << "File Succesfully Read! ";
	return true; //Success
}

bool loadPlayers(Player thePlayers[])
{
	//Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the input file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	// Loop through each row and column of the input file
	for(int c = 0; c < PLAYERS; c++)
	{
		ifs >> thePlayers[c].name;
		char tempLast[20];
		ifs >> tempLast;
		strcat(thePlayers[c].name, " ");
		strcat(thePlayers[c].name, tempLast);
	}

	//Close the file
	ifs.close();

	cout << "File Succesfully Read! ";
	return true; //Success
}

void setPlayerDeckDefaults(Player thePlayers[])
{
	for(int i = 0; i < PLAYERS; i++)
	{
		setDeckDefaults(thePlayers[i].hand, CARDS_PER_HAND); 	// Wipe players hand
		thePlayers[i].total = 0;								// Wipe players total
	}
}

void setDeckDefaults(Card deck[], int deckLength)
{
	for(int i = 0; i < deckLength; i++)
	{
		// Set default values
		strcopy(deck[i].suit, "suit");
		strcopy(deck[i].rank, "rank");
		strcopy(deck[i].location, "location");
		deck[i].cvalue = 0;
	}
}

bool shuffleDeck(Card shuffledDeck[], Card unShuffledDeck[])
{
	// Deep copy unShuffledDeck to shuffleDeck
	for(int i = 0; i < CARDS; i++)
		cardCopy(shuffledDeck[i], unShuffledDeck[i], "location");

	// Seed
	srand(time(0));

	// Loop through each card and shuffle it with a random index to swap ("shuffle") the cards
	for(int i = 0; i < CARDS; i++)
	{
		int j = rand()%CARDS; 			// Random index to shuffle
		Card temp;
		cardCopy(temp, shuffledDeck[i], "shuffled");
		cardCopy(shuffledDeck[i], shuffledDeck[j], "shuffled");
		cardCopy(shuffledDeck[j], temp, "shuffled");
	}
}

bool dealCards(Card shuffledDeck[], Player thePlayers[], Card stockPileDeck[], Card discardDeck[])
{
	int pos = 0;

	// Loop through every card to be dealt  (# of players * # of cards per hand)
	while(pos < PLAYERS * CARDS_PER_HAND)
	{
		// pos % PLAYERS (4) will cycle through each player
		// pos / PLAYERS (4) will increment the hand position each round robin
		cardCopy(thePlayers[pos%PLAYERS].hand[pos/PLAYERS], shuffledDeck[pos], thePlayers[pos%PLAYERS].name);	
		
		// Set the players total
		thePlayers[pos%PLAYERS].total += shuffledDeck[pos].cvalue; 
		// Increment position
		pos++; 
	}

	// Copy the next card into the discrard pile (following SCAT rules)
	cardCopy(discardDeck[0], shuffledDeck[pos], "discard");
	pos++; //Increment

	// Copy the remaining cards into stock pile
	for(int s = 0; pos < CARDS; s++, pos++)
	{
		cardCopy(stockPileDeck[s], shuffledDeck[pos], "stock");
	}
	return true;
}

void printPlayers(Player thePlayers[])
{
	for(int i = 0; i < PLAYERS; i++)
	{
		cout << "Player: " << thePlayers[i].name << " Total: " << thePlayers[i].total << endl;

		for(int c = 0; c < CARDS_PER_HAND; c++)
		{
			cout << "\tCard " << (c+1) << ": ";
			printCard(thePlayers[i].hand[c]);
			cout << endl;
		}
	}
}

void printAllDecks(Card shuffledDeck[], Card unShuffledDeck[], Card stockPileDeck[], Card discardDeck[])
{
	cout << "The shuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++)
	{
		cout << '\t';
		printCard(shuffledDeck[i]);
		cout << endl;
	}

	cout << "The stockPileDeck: " << endl;

	for(int i = 0; i < CARDS; i++)
	{
		cout << '\t';
		printCard(stockPileDeck[i]);
		cout << endl;
	}

	cout << "The discardDeck: " << endl;

	for(int i = 0; i < CARDS; i++)
	{
		cout << '\t';
		printCard(discardDeck[i]);
		cout << endl;
	}

	cout << "The unShuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++)
	{
		cout << '\t';
		printCard(unShuffledDeck[i]);
		cout << endl;
	}

}

void printCard(Card card)
{
	cout << card.suit << " " << card.rank;
	cout << " (" << card.cvalue << ") Location: " << card.location;
}

void cardCopy(Card &dest, Card &source, const char newLocation[])
{
	strcopy(dest.suit, source.suit);
	strcopy(dest.rank, source.rank);
	strcopy(dest.location, newLocation);
	strcopy(source.location, newLocation);
	dest.cvalue = source.cvalue;
}

void strcopy(char dest[], const char source[])
{
	// Start the source pointer at the beginning
	for(int i = 0; source[i] != '\0'; i++)
	{
		dest[i] = source[i]; // Copy the source character to the desination
		dest[i+1] = '\0';    // Erase the next character with the null character
	}	
}

void strcat(char dest[], const char source[])
{
	int pos = 0;
	while(dest[pos] != '\0')
		pos++;
	for(int i = 0; source[i] != '\0'; i++)
	{
		dest[pos+i] = source[i];
		dest[pos+i+1] = '\0';
	}
}