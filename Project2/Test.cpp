#include <iostream>

using namespace std;

struct Card
{
	char suit[7];
	char rank[7];
	int cvalue;
};


void swap ( Card * a, Card * b)
{
	Card * temp  = a;
	a = b;
	b = temp;
}


int main(int argc, char * argv[])
{
	Card array[2] = {{"heart", "seven", 7}, {"spade", "eight", 8}};
	// swap(array[0], array[1]);
	cout << array[0].suit << " " << array[1].suit << endl;
	return 0;
}
