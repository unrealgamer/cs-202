#include "listA.h"

// List constructor
List::List(int size)
{
	this->size = size;
	this->actual = 0;
	this->cursor = 0;
	this->data = new char[this->size];
}

// List Copy Constructor
List::List(const List& list)
{
	(*this) = list;
}

// List desconstructor
List::~List()
{
	delete [] this->data;
}

// Sets the cursor index to 0
bool List::gotoBeginning()
{
	this->cursor = 0;
	return true;
}

// Sets the cursor index to the last element in the list
bool List::gotoEnd()
{
	this->cursor = this->actual-1;
	return true;
}

// Moves the cursor to the next element in the list
bool List::gotoNext() 
{
	if(this->cursor == this->actual-1)
		return true;
	else
		this->cursor++;
	return true;
}

// Moves the cursor to the previous element in the list
bool List::gotoPrior() 
{
	if(this->cursor <= 0)
		return true;
	else
		this->cursor--;
	return true;
}

// Inserts the provided character into the list after the cursor
// and moves the cursor to the new element
bool List::insertAfter(char c) 
{
	if(full())
		return false;

	for(int i = this->actual-1; i > this->cursor; i--)
	{
		this->data[i+1] = this->data[i];
	}

	this->cursor++;

	if(!replace(c))
		return false;

	this->actual++;

	return true;
}

// Inserts the provided character into the list before the cursor
// and moves the cursor to the new element
bool List::insertBefore(char c) 
{
	if(full())
		return false;

	// if(!gotoPrior() && this->actual != 0)
	// 	return false;

	for(int i = this->actual-1; i >= this->cursor; i--)
	{
		this->data[i+1] = this->data[i];
	}

	if(!replace(c))
		return false;

	this->actual++;

	return true;
}

//// Removes the element at the cursor and shuffles the list
bool List::remove(char& o) 
{
	if(!getCursor(o))
		return false;

	for(int i = this->cursor; i < this->actual-1; i++)
	{
		this->data[i] = this->data[i+1];
	}

	if(this->cursor == this->actual-1)
		gotoBeginning();

	//this->data[this->cursor] = '\0';
	this->actual--;
	return true;
}

// Replaces the data at the cursor with the provided cursor
bool List::replace(char c) 
{
	this->data[this->cursor] = c;
	return true;
}

// Gets the data at the character and sets it to the provided argument
bool List::getCursor(char& o) const
{
	if(empty())
		return false;
	if(this->cursor < 0 || this->cursor >= this->actual)
		return false;
	o = this->data[this->cursor];
	return true;
}

// Checks if the list is empty
bool List::empty() const
{
	return this->actual == 0;
}

// The list has no limit so always return false
bool List::full() const
{
	return this->actual == size;
}	

// Empties the list by deleting all of the data
bool List::clear()
{
	this->cursor = 0; 
	this->actual = 0;
	delete [] this->data;
	this->data = new char[this->size];
	return true;	
}

// Assignment operator that deep copies the provided list
List& List::operator=(const List& list)
{

	this->cursor 	= list.cursor;
	this->size 		= list.size;
	this->actual 	= list.actual;

	delete [] this->data;

	this->data = new char[list.size];

	for(int i = 0; i < list.actual; i++)
	{
		this->data[i] = list.data[i];
	}

	return *this;
}

// Insertion operator that prints the list to the output stream
ostream& operator<<(ostream& out, const List& list)
{
	for(int i = 0; i <= list.actual-2; i++)
	{
		out << list.data[i] << ", ";
	}

	out << list.data[list.actual-1];

	//char c;

	//list.getCursor(c);

	//out << "\t\t Cursor: " << list.cursor << " (" << c << ")";

	return out;
}

// Comparison operator the compares the lists
bool List::operator==(const List& list) const
{

	if(this->cursor != list.cursor || this->size != list.size || this->actual != list.actual)
		return false;

	for(int i = 0; i < this->actual; i++)
		if(this->data[i] != list.data[i])
			return false;
	
	return true;
}