#include "listA.h"
#include <iostream>

using namespace std;

int main() {
	int size = 5;
	char c;

	List l(size);

	if(!l.gotoBeginning()) {
		cout << "Error in gotoBeginning" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.insertBefore('a'))
	{
		cout << "Error in insertBefore: a" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.insertBefore('b'))
	{
		cout << "Error in insertBefore: b" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.insertBefore('c'))
	{
		cout << "Error in insertBefore: c" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.gotoEnd())
	{
		cout << "Erro in gotoEnd" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.remove(c))
	{
		cout << "Error in remove" << endl;
	}

	cout << "Removed: " << c << endl;
	cout << "List: " << l << endl;

	if(!l.insertAfter('d'))
	{
		cout << "Error in insertAfter: d" << endl;
	}

	cout << "List: " << l << endl;

	if(!l.remove(c))
	{
		cout << "Error in remove" << endl;
	}

	cout << "Removed: " << c << endl;
	cout << "List: " << l << endl;

	if(!l.remove(c))
	{
		cout << "Error in remove" << endl;
	}

	cout << "Removed: " << c << endl;
	cout << "List: " << l << endl;

	if(!l.remove(c))
	{
		cout << "Error in remove" << endl;
	}

	cout << "Removed: " << c << endl;
	cout << "List: " << l << endl;

	return 0;
}