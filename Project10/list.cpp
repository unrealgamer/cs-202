#include "listN.h"


Node::Node(char c, Node * n)
{
	this->data = c;
	this->next = n;
}


List::List(int size)
{
	this->cursor = NULL;
	this->head = NULL;
}

List::List(const List& list)
{
	(*this) = list;
}

List::~List()
{
	clear();
}

bool List::gotoBeginning()
{
	this->cursor = this->head;
	return true;
}

bool List::gotoEnd()
{
	if(empty())
		return true;
	while(gotoNext());
	return true;
}

bool List::gotoNext() 
{
	if(empty())
		return true; 

	if(this->cursor->next != NULL)
		this->cursor = this->cursor->next;
	else
		return false;
	return true;
}

bool List::gotoPrior() 
{

	if(empty())
		return true;

	Node * temp = this->head;

	if(this->cursor == this->head)
		return true;

	while(temp->next != this->cursor)
		temp = temp->next;

	this->cursor = temp;

	return true;
}

bool List::insertAfter(char c) 
{

	if(empty())
	{
		this->head = new Node(c, NULL);
		this->cursor = this->head;
		return true;
	}

	if(this->cursor->next == NULL)
	{
		this->cursor->next = new Node(c, NULL);
		this->cursor = this->cursor->next;
		return true;
	}

	this->cursor->next = new Node(c, this->cursor->next);

	this->cursor = this->cursor->next;

	return true;
}

bool List::insertBefore(char c) 
{
	if(empty())
	{
		this->head = new Node(c, NULL);
		this->cursor = this->head;
		return true;
	}

	if(this->cursor == this->head)
	{
		this->head = new Node(c, this->cursor);
		this->cursor = this->head;
		return true;
	}

	gotoPrior();

	insertAfter(c);

	return true;
}

bool List::remove(char& o) 
{

	getCursor(o);

	if(this->cursor == this->head && this->cursor->next == NULL)
	{
		delete this->cursor;
		this->head = NULL;
		this->cursor = NULL;
		return true;
	}
	else if(this->cursor == this->head)
	{
		this->head = this->cursor->next;
		delete this->cursor;
		this->cursor = this->head;
		return true;
	}

	Node * nDelete = this->cursor;

	gotoPrior();

	this->cursor->next = nDelete->next;

	delete nDelete; nDelete = NULL;

	if(this->cursor->next == NULL)
		gotoBeginning();
	else
		this->cursor = this->cursor->next;


	return true;
}

bool List::replace(char c) 
{
	this->cursor->data = c;
	return true;
}

bool List::getCursor(char& o) const
{
	o = this->cursor->data;
	return true;
}

bool List::empty() const
{
	return this->head == NULL;
}

bool List::full() const
{
	return false;
}	

bool List::clear()
{
	if(empty())
		return true;

	gotoEnd();
	gotoPrior();

	while(this->cursor->next != NULL)
	{
		delete this->cursor->next;
		this->cursor->next = NULL;
		gotoPrior();
	}

	delete this->cursor;
	this->cursor = NULL;
	this->head = NULL;

	return true;	
}

List& List::operator=(const List& list)
{	

	Node * src = list.head;

	Node * tempCursor = NULL;

	this->head = new Node(list.head->data, NULL);
	this->cursor = this->head;

	while(src->next != NULL)
	{
		if(src == list.cursor)
			tempCursor = this->cursor;

		this->cursor->next = new Node(src->next->data, NULL);
		this->cursor = this->cursor->next;
		src = src->next;
	}

	this->cursor = tempCursor;

	return *this;
}

ostream& operator<<(ostream& out, const List& list)
{
	if(list.empty())
	{
		out << "Empty!";
		return out;
	}
	Node * temp = list.head;	
	while(temp->next != NULL)
	{
		out << temp->data << ", ";
		temp = temp->next;
	}

	out << temp->data;
	return out;
}

bool List::operator==(const List& list) const
{
	Node * org = list.head;
	Node * oth = this->head;

	while(org->next != NULL && oth->next != NULL)
	{

		if((list.cursor == org && this->cursor != oth) || (this->cursor == oth && list.cursor != org))
			return false;

		if(org->data != oth->data)
			return false;
		org = org->next;
		oth = oth->next;
	}

	if(oth->data != org->data)
		return false;

	return true;
}