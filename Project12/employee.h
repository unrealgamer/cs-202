class Employee : public Person {

public:
	Employee(const char * f = "first", const char * l = "last", const int age = 0, const int * s = NULL, const int * num = NULL,const char * t = "title", const int sal = 0);

	~Employee();

	const int * 	getEmployeeNumber() const;
	const char * 	getTitle() const;
	const int 		getSalary() const;

	void 			setEmployeeNumber(const int * e);
	void			setTitle(const char * t);
	void			setSalary(const int s);

	void 			print() const;

private:
	int * 	eNum;
	char * 	title;
	int 	salary;
};