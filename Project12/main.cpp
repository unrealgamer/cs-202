#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>

#include "person.h"
#include "employee.h"
#include "student.h"
#include "cstring.h"

using namespace std;

void stringToIntArray(const char * temp, int * tempList)
{
	for(int i = 0; i < strlen(temp); i++)
	{
		tempList[i] = temp[i] - '0';
	}
}

int main()
{
	// Use vectors to store pointers to create persons
	vector<Employee *> employees;
	vector<Student *> students;

	ifstream file("persons");

	// If the file is open read the persons in
	if(file.is_open())
	{
		string temp; //Temp String for values
		int * tempIntList = new int[10]; // Temp int array for values
		for(int i = 0; i < 18; i++)
		{
			getline(file, temp, ':'); // Read person type
			if(temp[0] == 'e') // Employee Type
			{
				Employee * e = new Employee();

				getline(file, temp, ':');	// Read and set first name
 				e->setFirstName(temp.c_str());

				getline(file, temp, ':');	// Read and set last name
				e->setLastName(temp.c_str());

				getline(file, temp, ':');	// Read and set age
				e->setAge(atoi(temp.c_str()));

				getline(file, temp, ':'); 	// Read and convert string to int array, then set
				stringToIntArray(temp.c_str(), tempIntList);
				e->setSSN(tempIntList);

				getline(file, temp, ':'); 	// Read and convert string to int array, then set
				stringToIntArray(temp.c_str(), tempIntList);
				e->setEmployeeNumber(tempIntList);

				getline(file, temp, ':');	// Read and set title
				e->setTitle(temp.c_str());

				getline(file, temp); 		// Read and set salary
				e->setSalary(atoi(temp.c_str()));

				employees.push_back(e);		// Add new employee to vector
			}
			else
			{
				Student * s = new Student();

				getline(file, temp, ':');	// Read and set first name
				s->setFirstName(temp.c_str());

				getline(file, temp, ':');	// Read and set last name
				s->setLastName(temp.c_str());

				getline(file, temp, ':'); 	// Read and set age
				s->setAge(atoi(temp.c_str()));

				getline(file, temp, ':');	// Read and convert string to int array, then set
				stringToIntArray(temp.c_str(), tempIntList);
				s->setSSN(tempIntList);

				getline(file, temp, ':');	// Read and convert string to int array, then set
				stringToIntArray(temp.c_str(), tempIntList);
				s->setNSHENumber(tempIntList);

				getline(file, temp, ':'); 	// Read and set major
				s->setMajor(temp.c_str());

				getline(file, temp);		// Read and set gpa
				s->setGPA(strtod(temp.c_str(), NULL));

				students.push_back(s); // Add new student to vector
			}
		}	
		// Clean up some memory
		delete [] tempIntList;
	}

	// Print the employees for verification
	for(uint i = 0; i < employees.size(); i++)
	{
		employees[i]->print(); 

		cout << endl;
	}

	// Print the students for verification 
	for(uint i = 0; i < students.size(); i++)
	{
		students[i]->print();
		cout << endl;
	}

	//-------------- TESTS ---------------- //

	//  1 Base Class Pointer //
	cout << "TEST 1:" << endl;
	Person * person = employees[0];
	person->print(); cout << endl;
	cout << endl;

	// Results: Calls the super class' ("Person") print method
	// 			not the sub class ("Employee")

	//  2 Constuctor Order of Invocation //
	cout << "TEST 2: " << endl;
	Student * testStudent = new Student();
	cout << endl;

	// Results: Person constructor is executed first then the
	//			Student constructor is executed

	//  3 Deconstructor Order of Invocation //
	cout << "TEST 3: " << endl;
	delete testStudent;
	cout << endl;

	// Results: Student deconstructor is executed first then
	//			Person deconstrucotor is executed

	//  4 Pointer of type Student for Employee
	cout << "TEST 4: " << endl;
	//  testStudent = employees[0];
	cout << "Will Cause Error, Please See Comments";
	cout << endl << endl;

	//  Results: Error:
	// 			 Invalid conversion from Employee* to Student*

	// --------------- END TEST --------------- //

	return 0;
}