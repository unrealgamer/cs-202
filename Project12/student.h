class Student : public Person {

public:
	Student(const char * f = "first", const char * l = "last", const int age = 0, const int * s = NULL, const int * num = NULL,const char * m = "major", const double g = 0);

	~Student();

	const int * 	getNSHENumber() const;
	const char * 	getMajor() const;
	const double 	getGPA() const;

	void 			setNSHENumber(const int * n);
	void			setMajor(const char * t);
	void			setGPA(const double s);

	void 			print() const;

private:
	int * 	NSHENumber;
	char * 	major;
	double 	gpa;
};