#include <iostream>
#include "person.h"
#include "employee.h"
#include "cstring.h"

using namespace std;

Employee::Employee(const char * f, const char * l, const int age, const int * s, const int * num, const char * t, const int sal)
	: Person(f, l, age, s)
{
	cout << "Employee Constructed!" << endl;
	// Dynamically Create new C-Strings for members

	if(t == NULL)
		title = createNewString("first");
	else
		title = createNewString(t);

	// Set the salary
	salary = sal;

	// Dynamically create new int array and populate it 
	// if an array is provided

	if(num == NULL)
		eNum = new int[5];
	else
	{
		eNum = new int[5];
		for(int i = 0; i < 5; i++)
			eNum[i] = num[i];
	}
}

Employee::~Employee()
{
	delete [] title;
	delete [] eNum;
	title = NULL;
	eNum = NULL;
}

const char * Employee::getTitle() const
{
	return title;
}

const int Employee::getSalary() const
{
	return salary;
}

const int * Employee::getEmployeeNumber() const
{
	return eNum;	
}

void Employee::setTitle(const char * t)
{
	// If the new title is null return
	if(t == NULL)
		return;

	// If the new title is smaller than current we must resize
	if(strlen(t) > strlen(title))
	{
		delete [] title;
		title = createNewString(t);
	}
	else // Otherwise just copy
	{
		strcopy(title, t);
	}
}

void Employee::setSalary(const int sal)
{
	salary = sal;
}

void Employee::setEmployeeNumber(const int * num)
{
	//If the new number is null then return
	if(num == NULL)
		return;
	// Otherwise just copy
	for(int i = 0; i < 5; i++)
		eNum[i] = num[i];
}

void Employee::print() const
{
	//Print the employee data members
	cout  << "Employee: ";
	Person::print();

	cout << "  ENum: ";
	for(int i = 0; i < 5; i++)
		cout << eNum[i];

	cout << "  Title: " << title << "  Salary: $" << salary; 

	cout << endl;
}