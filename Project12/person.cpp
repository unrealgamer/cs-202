#include <iostream>
#include "person.h"
#include "cstring.h"

using namespace std;

Person::Person(const char * f, const char * l, const int a, const int * s)
{
	cout << "Person Contructed!" << endl;

	// Dynamically Create new C-Strings for members

	if(f == NULL)
		fname = createNewString("first");
	else
		fname = createNewString(f);
	if(l == NULL)
		lname = createNewString("last");
	else
		lname = createNewString(l);

	// Set the age
	age = a;

	// Dynamically create new int array and populate it 
	// if an array is provided

	if(s == NULL)
		ssn = new int[9];
	else
	{
		ssn = new int[9];
		for(int i = 0; i < 9; i++)
			ssn[i] = s[i];
	}

}

Person::~Person()
{
	cout << "Person Deconstructed!" << endl;
	delete [] fname;
	delete [] lname;
	delete [] ssn;
	fname = lname = NULL;
	ssn = NULL;
}

const char * Person::getFirstName() const
{
	return fname;
}

const char * Person::getLastName() const
{
	return lname;
}

const int Person::getAge() const
{
	return age;
}

const int * Person::getSSN() const
{
	return ssn;
}

void Person::setFirstName(const char * f)
{
	// If the new name is null return
	if(f == NULL)
		return;

	// If the new name is smaller than current we must resize
	if(strlen(f) > strlen(fname))
	{
		delete [] fname;
		fname = createNewString(f);
	}
	else // Otherwise just copy
	{
		strcopy(fname, f);
	}
}

void Person::setLastName(const char * l)
{
	// If the new name is null return
	if(l == NULL)
		return;

	// If the new name is smaller than current we must resize
	if(strlen(l) > strlen(lname))
	{
		delete [] lname;
		lname = createNewString(l);
	}
	else // Otherwise just copy
	{
		strcopy(lname, l);
	}
}

void Person::setAge(const int a)
{
	age = a;
}

void Person::setSSN(const int * s)
{
	// If the new ssn is null return
	if(s == NULL)
		return;

	// Otherwise just copy
	for(int i = 0; i < 9; i++)
		ssn[i] = s[i];
}

void Person::print() const
{ 
	// Print out the members
	cout << fname << " " << lname;
	cout << " (SSN: ";

	for(int i = 0; i < 9; i++)
		cout << ssn[i];

	cout << "  Age: " << age << ")"; 
}