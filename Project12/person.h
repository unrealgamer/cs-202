class Person {

public:
	Person(const char * f = "first", const char * = "last", const int age = 0, const int * s = NULL);

	~Person();

	const char * 	getFirstName() const;
	const char * 	getLastName() const;
	const int		getAge() const;
	const int * 	getSSN() const;

	void 	setFirstName(const char * f);
	void	setLastName(const char * l);
	void	setAge(const int a);
	void	setSSN(const int * ssn);

	void	print() const;

private:
	char * 	fname;
	char * 	lname;
	int 	age;
	int *	ssn;
};