#include <iostream>
#include "person.h"
#include "student.h"
#include "cstring.h"

using namespace std;

Student::Student(const char * f, const char * l, const int age, const int * s, const int * num, const char * m, const double g)
	: Person(f, l, age, s)
{
	cout << "Student Contructed!" << endl;

	// Dynamically Create new C-Strings for members

	if(m == NULL)
		major = createNewString("first");
	else
		major = createNewString(m);

	// Set the gpa
	gpa = g;

	// Dynamically create new int array and populate it 
	// if an array is provided

	if(num == NULL)
		NSHENumber = new int[10];
	else
	{
		NSHENumber = new int[10];
		for(int i = 0; i < 10; i++)
			NSHENumber[i] = num[i];
	}
}

Student::~Student()
{
	cout << "Student Deconstructed!" << endl;
	delete [] major;
	delete [] NSHENumber;
	major = NULL;
	NSHENumber = NULL;
}

const char * Student::getMajor() const
{
	return major;
}

const double Student::getGPA() const
{
	return gpa;
}

const int * Student::getNSHENumber() const
{
	return NSHENumber;	
}

void Student::setMajor(const char * m)
{
	// If the new major is null then return
	if(m == NULL)
		return;
	// If the new major is smaller than current we must resize
	if(strlen(m) > strlen(major))
	{
		delete [] major;
		major = createNewString(m);
	}
	else //Otherwise just copy
	{	
		strcopy(major, m);
	}
}

void Student::setGPA(const double g)
{
	gpa = g;
}

void Student::setNSHENumber(const int * num)
{
	//If the new number is null then return
	if(num == NULL)
		return;
	//Otherwise just copy
	for(int i = 0; i < 10; i++)
		NSHENumber[i] = num[i];
}

void Student::print() const
{
	// Print out the student data members
	cout  << "Student: ";
	Person::print();

	cout << "  NSHENumber: ";
	for(int i = 0; i < 10; i++)
		cout << NSHENumber[i];

	cout << "  Major: " << major << "  GPA: " << gpa; 

	cout << endl;
}