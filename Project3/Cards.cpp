/***************************************************************/
/* PROGRAM: Project 3 Card Shuffler                                                                  */
/*                                                                                       */
/* AUTHOR:  Shane Melton                                                                    */
/*                                                                                       */
/* DATE:    September 16, 2014                                                                        */
/*                                                                                      */
/* REVISIONS: 1                                                                */
/*                                                                                      */
/* PURPOSE: A small program that will read a deck of 52 cards and shuffle them. */
/*                                                                                      */
/**************************************************************/
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

// Constants for the number of cards in a deck, number of players

const int CARDS = 52;
const int PLAYERS = 4;

// A struct that represents a custom Card datatype
struct Card
{
	char 	suit[20];
	char 	rank[20];
	int 	cvalue;
	char 	location[30];
};

// A struct that represents a custom Player datatype
struct Player
{
	char 	name[50];
	int 	total;
};

// Function Prototypes

/**
	Prints the menu to the console. (Does not wait for any user input)
*/
void printMenu();

/**
	Loads a deck of cards from a file and stores them into the single dimensiaonal array provided
	@param Card* The array to be filled with cards
*/
bool loadUnshuffledDeck(Card *);

/**
	Loads a list of players from a file and stores them into the single dimensiaonal array provided
	@param Player* The array to be filled with players
*/
bool loadPlayers(Player *);

/**
	Resets the deck (array) by filling it with a default card value
	@param Card* The deck to reset
	@param deckLength The length of the provided deck
*/
void setDeckDefaults(Card *, int deckLength = CARDS);

/**
	Takes cards from the unshuffled deck (which never changes after load) and randomly orders
	them within the shuffled deck.
	@param shuffledDeck* The deck to be shuffled
	@param unShuffledDeck* The static deck used to populate the shuffled deck
*/
bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck);

/**
	Prints a card and all of its attributes to the console
	@param Card The card to print
*/
void printCard(Card);

/**
	Prints all of the players to screen including their hand and other attributes
	@param Player* The array of players
*/
void printPlayers(Player *);

/**
	Prints every deck to screen
	@param Self explanatory
*/
void printAllDecks(Card * shuffledDeck, Card * unShuffledDeck );

/**
	Does a deep copy of the source card into the destination. Also the card has its location set
	to the provided new location C string.
	@param dest The destination card
	@param source The source card
	@param newLocation The new location for the card (typically the destination deck)
*/
void cardCopy(Card &dest, Card &source, const char *  newLocation);

/**
	Concatenates two C style strings together which is stored in the dest argument
	@param dest The destination for the concatenation and the first portion of the concatenated string
	@param source The source for the concatentation that will be appended to the destination string
*/
void strcat(char * dest, const char * source);

/**
	Copys the conents from the source C string to the destination C string
	@param dest The destination for the source to be copied too
	@param source The source to be copied	
*/
void strcopy(char * dest, const char * source);


// End Function Prototypes

using namespace std;

// Main entry point of the program
int main() {

	// Integer used to store the users selection in the menus
	int userSel = 0;

	// Booleans to check if the deck and players were loaded succesfully
	bool loadedDeck = false;
	bool loadedPlayers = false;

	// Array for deck and players
	Card unShuffledDeck[CARDS];
	Card shuffledDeck[CARDS];

	// Sets every card in each deck to the default card
	setDeckDefaults(unShuffledDeck);
	setDeckDefaults(shuffledDeck);

	Player thePlayers[PLAYERS];

	while(userSel != 6)
	{
		printMenu(); // Print the menu to the screen

		cin >> userSel; // Wait for the user's input

		if(!cin.good()) // Verify the user's input is an integer
		{
			cout << "Improper Selection! Please try again!" << endl;

			cin.clear(); // Clear the input buffer

			cin.ignore(); // Flush the new line character

			continue;
		}

		cin.ignore(); // Flush the new line character '\n' from the buffer

		switch(userSel)
		{
			case 1: // Load Deck File
			{
				loadedDeck = loadUnshuffledDeck(unShuffledDeck);
				cout << endl;
				break;
			}
			case 2: // Load Player File
			{
				loadedPlayers =	loadPlayers(thePlayers);
				cout << endl;
				break;
			}
			case 3: // Shuffles the deck
			{
				if(loadedDeck)
				{
					shuffleDeck(shuffledDeck, unShuffledDeck);
					cout << "Deck Shuffled Succesfully!" << endl;
				}
				else
					cout << "Please load the deck first. (Option 1)" << endl;
				break; 
			}
			case 4: // Print Players and Hands
			{
				if(loadedDeck && loadedPlayers)
					printPlayers(thePlayers);
				else
					cout << "Please load the deck and players first. (Option 1 & 2)" << endl;
				break;
			}
			case 5: // Print all decks to screen
			{
				if(loadedDeck && loadPlayers)
					printAllDecks(shuffledDeck, unShuffledDeck); 
				else
					cout << "Please make sure everything is loaded and atleast one shuffle has occured." << endl;
				break;
			}
			case 6: // Exit the program
			{
				cout << "Good Bye" << endl;
				return 0;
			}
			default: // Out of range option
			{
				cout << "Please select an option 1-6." << endl;
				break;
			}
		}
		cout << "Press the enter key to continue...";
		cin.ignore();
	}
	return 0;
}


void printMenu()
{
	cout << endl << endl << "Select an option below: " << endl;

	cout << "1. Load Card File" << endl;
	cout << "2. Load Player File" << endl;
	cout << "3. Shuffle Deck" << endl;
	cout << "4. Print Players" << endl;
	cout << "5. Print All Decks" << endl;
	cout << "6. Exit" << endl;

	cout << "Selection: ";
}

bool loadUnshuffledDeck(Card * unShuffledDeck)
{
	//Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the input file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	Card * cptr = unShuffledDeck;

	// Loop through each card and increment the count and pointer
	for(int i = 0; i < CARDS; cptr++, i++)
	{
		ifs >> (*cptr).suit;
		ifs >> (*cptr).rank;
		ifs >> (*cptr).cvalue;
		strcopy((*cptr).location, "unshuffled");
	}

	//Close the file
	ifs.close();

	cout << "File Succesfully Read! ";
	return true; //Success
}

bool loadPlayers(Player * thePlayers)
{
	//Ask the user for a file name and store it in a C String
	char fileName[50];

	cout << "Enter the input file name: ";

	cin.getline(fileName, 50);

	// Create a file stream using the fileName
	ifstream ifs (fileName);

	// Verify the file opened correctly
	if(!ifs.is_open())
	{
		cout << "There was an error reading the file: " << fileName << endl;
		cout << "Please try again" << endl;
		return false; //Failed
	}

	Player * pptr = thePlayers;

	// Loop through each player and increment the count and pointer
	for(int i = 0; i < PLAYERS; pptr++, i++)
	{
		ifs >> (*pptr).name;
		char tempLast[20];
		ifs >> tempLast;
		strcat((*pptr).name, " ");
		strcat((*pptr).name, tempLast);
	}

	//Close the file
	ifs.close();

	cout << "File Succesfully Read! ";
	return true; //Success
}

void setDeckDefaults(Card * deck, int deckLength)
{
	// Card Pointer for iterating through the deck
	Card * cptr = deck;

	// Loop through and increment pointer and counter
	for(int i = 0; i < deckLength; cptr++, i++)
	{
		// Set default values
		strcopy((*cptr).suit, "suit");
		strcopy((*cptr).rank, "rank");
		strcopy((*cptr).location, "location");
		(*cptr).cvalue = 0;
	}
}

bool shuffleDeck(Card * shuffledDeck, Card * unShuffledDeck)
{
	// Create pointers for the shuffled and unshuffled decks
	Card * shuPtr = shuffledDeck;
	Card * unsPtr = unShuffledDeck;

	// Deep copy unShuffledDeck to shuffleDeck (for loop increments pointers and counter)
	for(int i = 0; i < CARDS; i++, shuPtr++, unsPtr++)
		cardCopy((*shuPtr), (*unsPtr), "location");

	// Seed
	srand(time(0));

	// Reset Shuffled Pointer
	shuPtr = shuffledDeck;

	// Loop through each card and shuffle it with a random index to swap ("shuffle") the cards
	for(int i = 0; i < CARDS; shuPtr++, i++)
	{
		int j = rand()%CARDS; 						// Random index to shuffle
		Card * otherPtr = shuffledDeck; 			// Other Card pointer used for swapping
		for(int k = 0; k < j; k++, otherPtr++);	// Increment otherPtr to the random index
		Card temp;									// Temp card for swapping
		cardCopy(temp, *otherPtr, "shuffled");		// Perform the swap
		cardCopy(*otherPtr, *shuPtr, "shuffled");
		cardCopy(*shuPtr, temp, "shuffled");
	}
}

void printPlayers(Player * thePlayers)
{
	// Card pointer for interating through the deck
	Player * pPtr = thePlayers;
	for(int i = 0; i < PLAYERS; i++, pPtr++)
	{
		cout << "Player: " << (*pPtr).name << " Total: " << (*pPtr).total << endl;
	}
}

void printAllDecks(Card * shuffledDeck, Card * unShuffledDeck)
{
	// Card Pointer used for iterating through the card arrays
	Card * cPtr = shuffledDeck;

	cout << "The shuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		cout << '\t';
		printCard((*cPtr)); // Print the card
		cout << endl;
	}

	// Set the pointer to the other array
	cPtr = unShuffledDeck;

	cout << "The unShuffledDeck: " << endl;

	for(int i = 0; i < CARDS; i++, cPtr++)
	{
		cout << '\t';
		printCard((*cPtr)); // Print the card
		cout << endl;
	}

}

void printCard(Card card)
{
	cout << card.suit << " " << card.rank;
	cout << " (" << card.cvalue << ") Location: " << card.location;
}

void cardCopy(Card &dest, Card &source, const char * newLocation)
{
	// Copy each source to the respective destination
	strcopy(dest.suit, source.suit);
	strcopy(dest.rank, source.rank);
	strcopy(dest.location, newLocation);
	strcopy(source.location, newLocation);
	dest.cvalue = source.cvalue;
}

void strcat(char * dest, const char * source)
{
	// Move the destination pointer to the end of the C string
	while(*dest) // '\0' character will evalulate to false
		dest++; 
	// Start the source at the beginning 
	while(*source)
	{
		*dest = *source; // Copy the source character to the destination
		dest++; // increment pointer 
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}

void strcopy(char * dest, const char * source)
{
	// Start the source pointer at the beginning
	while(*source)
	{
		*dest = *source; // Copy the source character to the desination
		dest++; // increment pointer
		source++; // increment pointer
	}
	*dest = 0; // Set the null terminator
}