#include <iostream>

using namespace std;

struct Card
{
	char suit[7];
	char rank[7];
	int cvalue;
};


void swap ( Card * a, Card * b)
{
	Card * temp  = a;
	a = b;
	b = temp;
}


int main(int argc, char * argv[])
{

	Card * const MyArray = new Card[2];

	Card * cPtr = MyArray;

	(*cPtr).cvalue = 4;
	(*(cPtr++)).cvalue = 5;

	cout << (*MyArray).cvalue << endl;
	cout << (*cPtr).cvalue << endl;


	return 0;
}
